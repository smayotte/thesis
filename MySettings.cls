% -------------------------------------------------------------------
%  \linebreak[4]           to brake the line with filling             oder          \protect\linebreak
%  \unslant                to rotate math symbols
%  \textrm{
%  @LaTeX-class-file{
%     filename        = "MySettings.cls",
%     date            = "23 August 2017",
%     made by         = Ievgenii Kres 
% --------------------------------------------------------------------

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{MySettings}[2017/08/23  University Thesis Class]
\LoadClass[
%	BCOR=12mm,
%	paper=a4
	open=right,
	fontsize=12pt,
	cleardoublepage=empty,
	bibliography=notoc,
	numbers=noenddot,       % no dot after figure/table number
	captions=tableheading,  % correct spacing for table headings
%	titlepage=firstiscover, % symmetrical margins on title page
	headsepline,
	headings=normal,        % size of chapter headings slightly smaller
	]{scrbook}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[
%	showframe,
	a4paper,
	width=161mm,
	top=25mm,
	bottom=25mm,
	bindingoffset=12mm]{geometry}

\usepackage{lmodern}

% Required packages
\RequirePackage{graphicx}
\RequirePackage[colorlinks]{hyperref}
\hypersetup{
	linktocpage,
	linktoc=all,
    colorlinks=true,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=blue,
%    urlcolor=black,
}


\usepackage[british]{babel}
\usepackage[svgnames]{xcolor}
\usepackage{tikz}
\usepackage{csquotes}
\usepackage{caption}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{xfrac}
\usepackage{wasysym}
\usepackage{longtable}
\usepackage{adjustbox}
\usepackage{multirow}
%\usepackage{slashbox}
\usepackage{url}
%\usepackage{subfig}
\usepackage{subcaption}
\captionsetup[subfigure]{list=false}
\usepackage[titles]{tocloft}
\setcounter{tocdepth}{3}
\setcounter{secnumdepth}{4}
\cftpagenumbersoff{subfigure}
\usepackage[nameinlink]{cleveref}
%\usepackage[dvipsnames]{xcolor}
\usepackage{float}
% float package
\floatplacement{figure}{htbp}
\floatplacement{table}{htbp}
\captionsetup{
	labelfont=bf,
	font=small,
	width=0.9\textwidth,
}
\usepackage[section]{placeins}
\RequirePackage{color}
\renewcommand{\contentsname}{}
\renewcommand\bibname{References}

\usepackage[
	locale=US,
	separate-uncertainty=true,
	per-mode=symbol-or-fraction,
	]{siunitx}
\sisetup{math-micro=\text{µ},text-micro=µ}

\usepackage[
%	sorting=nty,
	sorting=none,
	style=numeric,
	backend=biber,
	autolang=hyphen,
	giveninits=true
	]{biblatex}
\addbibresource{front-and-backmatter/references3.bib}

\RequirePackage{url}
\RequirePackage[palatino]{quotchap}
\definecolor{chapterblue}{rgb}{0,0.1,0.6}%
\makeatletter
\renewcommand{\sectfont}{\bfseries}
\renewcommand{\chapnumfont}{
	\usefont{T1}{\@defaultcnfont}{b}{n}\fontsize{50}{70}\selectfont
	\color{chapterblue}
}
\makeatother

\usepackage{scrlayer-scrpage} 
\setheadsepline{0.5pt}[\color{chaptergrey}]

\renewcommand*{\chapterheadstartvskip}{\vspace*{-3\baselineskip}}
\renewcommand*{\chapterheadendvskip}{\vspace{1.3\baselineskip}}
\makeatletter
\let\size@chapter\LARGE
\makeatother
\RequirePackage{titling}
\RequirePackage{setspace} 
\RequirePackage{booktabs} % for much better looking tables
\onehalfspacing
\usepackage{graphbox}
\usepackage{epigraph}
\usepackage{braket}
\usepackage{upgreek}

% some definitions
\def\degreeyear#1{\gdef\@degreeyear{#1}}
\def\degreemonth#1{\gdef\@degreemonth{#1}}
\def\degree#1{\gdef\@degree{#1}}
\def\advisor#1{\gdef\@advisor{#1}}
\def\department#1{\gdef\@department{#1}}
\def\field#1{\gdef\@field{#1}}
\def\university#1{\gdef\@university{#1}}
\def\universitycity#1{\gdef\@universitycity{#1}}
\def\universitystate#1{\gdef\@universitystate{#1}}

%
%\renewcommand{\maketitle}{ 
%	\singlespacing
%	\thispagestyle{empty}
%	\vspace*{\fill} \vspace{150pt} \begin{center}
%	\Huge {\textit{\thetitle}} \normalsize \\ \sc \vspace{100pt}
%	a dissertation presented \\ by\\
%	\theauthor\\ to\\ The \@department\\ 
%	\vspace{12pt}
%	in partial fulfillment of the requirements\\ 
%	for the degree of\\ \@degree\\ 
%	in the subject of\\ \@field\\
%	\vspace{12pt}
%	\@university\\ \@universitycity, \@universitystate\\ 
%	\@degreemonth\ \@degreeyear
%	\end{center} \vspace*{\fill}
%}

% Start pages
\newcommand{\ThesisStart}{
	\newpage
	\pagenumbering{arabic}
}
