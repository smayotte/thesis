\chapter[COSMIC RAYS AND EXTENSIVE AIR SHOWERS]{Cosmic Rays and Extensive Air Showers}\label{chap:UHECRs}
\epigraph{\itshape\myopeningquote I canna' change the laws of physics, Captain! A've got to have thirty minutes.\myclosingquote}{--- Montgomery ``Scotty'' Scott, \textit{Star Trek: The Original Series}}

The existence of CRs was first hypothesized at the start of the 1900s, shortly after the discovery of radioactivity by Henri Beqcuerel in 1896 \cite{Becquerel}.
In 1900 and 1901 Julius Elster and Hans Friedrich Geitel \cite{Geitel, ElsterGeitel}, and independently, Charles Thomson Rees Wilson \cite{Wilson} discovered that a residual radioactivity in the surrounding environment was the cause for the spontaneous ionization of air in isolated chambers, such as in an electroscope.
As a direct result of these experiments the isolation of electroscopes was improved.
In additional experiments, Elster and Geitel discovered a drop in ionization when taking their experiment below ground to the bottom of a salt mine.
They concluded that, while the Earth seems to be the source of the radiation, certain soils and waters can act as a screening mechanism.

Between 1909 and 1910 Theodor Wulf performed experiments, with electroscopes he had further improved, on top of the Eiffel tower ($\approx\SI{300}{\meter}$) and compared the results to ground level measurements.
From the Elster and Geitel experiments, the believe at this time was that the radiation causing ionization was coming from the upper Earth layer.
Wulf did measure a drop in ionization with increased height.
However, the decrease was too small to support the hypothesis of radiation coming from the Earth, as an exponential decrease would be expected.
He attributed these variations in $\gamma$-radiation to differences in air pressure and deemed effects of radiation from the atmosphere to be too small to be detectable with his methods \cite{Wulf1, Wulf2}.

The final breakthrough and discovery of CRs came in 1912, when Victor Hess conducted seven balloon flights up to heights of \SI{5200}{\meter}.
His results with electroscopes showed that the ionization, after reaching a minimum, increased again considerably with height, from which he concluded that this increase must originate from radiation coming from above and is possibly of extra-terrestrial origin \cite{Hess}.
%Ever since then cosmic ray physics has been an import part of astroparticle research.

The term cosmic rays is used nowadays to describe high energy charged particles that traverse the universe at very nearly the speed of light.
CRs, together with photons, neutrinos, and dark matter form the core of the modern field of research, astroparticle physics.
CRs, photons, and neutrinos, combined with gravitation waves are the dominant phenomena studied in the field of multi-messenger physics.
In contrast to photons and neutrinos, which naturally point back to their source, the paths of CRs are heavily affected by both \emph{Galactic} and \emph{extragalactic magnetic fields} due to their charge.
This results in their flux at Earth being measured as nearly isotropic at all except the very highest energies, diminishing the possibility of identifying their source.
Additionally, at high energies their flux becomes so low that they cannot be directly detected in space and have to be observed from Earth.
This makes determining their composition very difficult, as they generally break up in the upper atmosphere.
This provides a very challenging and interesting environment for astroparticle researchers, as the composition of CRs is fundamental to the understanding of the processes with which they are accelerated.
This knowledge in turn would help to better the understanding of the high energy universe, since UHECRs are the highest energy particles ever observed.

\section{The Flux of Cosmic Rays}\label{sec:spectrum}
The energy spectrum of CRs relates their flux at Earth to their energy and is a powerful tool for understanding CR sources and acceleration mechanisms through the features it displays.
The all-particle energy spectrum of CRs as observed by air shower experiments from the TeV range up to the highest energies of \SI{3e20}{\electronvolt} is shown in \Cref{fig:spectrum}.
Generally the flux can be described by a broken power law, where each part of the spectrum can be described with the form
\begin{align}
	\frac{dN}{dE}\propto E^{-\gamma}\, ,
\end{align}
where $\gamma$ is the spectral index.
The changes in $\gamma$ in the different energy regions of the spectrum are thought to occur due to changes in composition, sources, or propagation.
To help highlight features in the spectrum, the flux is usually scaled with the energy, in the case of \Cref{fig:spectrum} a factor of $E^{2.6}$ is used.
% to make features more discernible.

\subsection{Features in the Cosmic Ray Energy Spectrum}\label{sec:SpectrumFeatures}
At the lowest energies, the flux of CRs is high enough to be directly observed at high altitudes or in space, allowing for precise composition measurements.
Up to energies in the GeV range, the majority of detected CRs originate from the sun, as low energy galactic CRs are mostly blocked from the solar system by the termination shock which forms when the solar wind meets the \emph{interstellar medium}.
Beyond a few GeV, the sun reaches its maximum acceleration power for CRs and its contribution to the spectrum drops off.
With increasing energy, direct measurements of CR composition have shown that galactic sources dominate the flux, revealing a composition which closely follows the atomic abundances found in stars.
A slight tendency toward lighter elements is measured, most importantly in B, Be and Li, which are not produced in stellar nucleosynthesis.
Instead, these are almost entirely produced by spallation of the plentiful C, N, or O CRs, on atoms of interstellar media, resulting in the loss of 1-2 protons.
In these regions the CR spectrum is well described by a power law of spectral index of $\gamma\sim 2.7$.
With increasing energy the sources are believed to shift to \emph{supernovae} and \glspl{snr}.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.8\textwidth]{figures/02_CosmicRaysAndExtensiveAirshowers/spectrum.pdf}
	\caption[All particle energy spectrum of cosmic rays]{All particle energy spectrum of CRs, as measured by various different air-shower experiments over several decades of energy. Most prominent features of the spectrum are labeled at their respective energies. Taken from \cite{pdg}.}
	\label{fig:spectrum}
\end{figure}
\noindent

At around \SI{3e15}{\electronvolt}, a steepening of the spectrum occurs, referred to as the \emph{knee}, which is shown in \Cref{fig:spectrum}.
This is believed to represent the maximum acceleration energy galactic sources can reach for protons.
After that, galactic accelerators reach their maximum energies for higher and higher charges and the composition transitions from light to heavy.
The behavior in this region is less stable, but can still be well described by a power law with $\gamma\sim 3.1$.
A second steepening occurs at around \SI{e17}{\electronvolt} which is closely related to these interpretations.
It is referred to as the \emph{second knee}, and is theorized to be where this maximum acceleration effect described earlier occurs for iron.
There, the spectral index changes to $\gamma\sim 3.3$.
Based on data from various experiments it is believed that these changes in spectral index of the spectrum occur at the same rigidity for each of the differently charged particles, and therefore occur at different energies \cite{pdg}.
This is around the last point where a grasp of the acceleration mechanisms and probable sources of CRs still exists, in the form of acceleration in supernovae and SNRs via first and second order Fermi acceleration \cite{fermi-acc, fermi-acc2}.
Additionally, around this energy cosmic rays are no longer confined to the Galaxy by the Galactic magnetic field which may also contribute to the steepening of the spectrum.
An in-depth description of galactic sources and acceleration will not be covered here, but can be found in  \cite{boo:AstPhyGrup}.
	
At energies beyond \SI{e17}{\electronvolt}, the flux of CRs enters a transition range, where possible galactic source candidates become few, and CRs from extragalactic sources likely start to contribute a significant fraction of the observed flux. 
As mentioned earlier, the spectral index here is well characterized by $\gamma\sim 3.3$, with this region extending up to energies of around \SI{5e18}{\electronvolt}.
At this point, the spectrum hardens noticeably to a spectral index of $\gamma\sim 2.5$, creating what is referred to as the \emph{ankle}.
Slightly below the ankle the composition of CRs is light and has a significant proton fraction.
As energy increases the composition shifts to increasingly heavier primaries \cite{AugerCompositionI, AugerCompositionII, recentXmax} and extragalactic sources are believed to dominate the flux.

The most recent measurements from the Pierre Auger Observatory can be seen in \Cref{fig:auger-spectrum} and clearly show this behavior of the flux at the ankle and beyond \cite{SpectrumMeasurement, SpectrumFeatures}.
The composition of the regions shown in \Cref{fig:auger-spectrum} is of particular interest to this thesis and will be explained in more detail in \cref{sec:composition}.
Additional features are visible, with a newly found softening of the spectrum to $\gamma\sim 3.05$ at around \SI{13}{\exa\electronvolt}, followed by an even stronger softening at \SI{46}{\exa\electronvolt} to a spectral index of $\gamma\sim 5.1$, which is commonly referred to as the \emph{cutoff}.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/02_CosmicRaysAndExtensiveAirshowers/auger-spectrum.png}
	\caption[Auger spectrum above \SI{2.5e18}{\electronvolt}]{All particle energy spectrum of CRs, as measured by the Pierre Auger Observatory at the highest energies around and above the ankle. Numbered spectral indices $\gamma_{i}$ correspond to numbered regions. Taken from \cite{SpectrumFeatures}.}
	\label{fig:auger-spectrum}
\end{figure}
\noindent

\subsection{The Cutoff of the Cosmic Ray Energy Spectrum}
The phenomenological reason for the cutoff is a matter of current debate and intense research.
Due to the belief that the spectrum of CRs is, at these energies, purely extragalactic in source, the origin of the cutoff is anticipated to be extragalactic as well.
There are a few possibilities currently being investigated as the probable cause:

\begin{description}
	\item[Maximum Rigidity:] This effect would be similar in nature to the one described above for the knee and second knee, only this time for extragalactic CRs, where sources accelerate particles to maximum energies according to their charge, \ie to the same maximum rigidity.
	
	If the flux around the ankle is proton dominated, this scenario would lead to the proton contribution in the spectrum dropping off after their maximum rigidity is reached and gradually shift to heavier and heavier components.
	These would similarly drop off at the same maximum rigidity, which is shifted in energy according to their charge $Z$.
	In this scenario, the ultra-high energy end of the all-particle spectrum before the cutoff would be dominated by the heaviest available component, \ie the iron group.
	The suppression of the flux at the highest energies would then indicate a cutoff of the source spectrum, where extragalactic sources of UHECRs have reached this maximum rigidity to which they are able to accelerate particles even for the heaviest components \cite{CombinedFit}. 

	\item[GZK Limit:] This effect was first theorized by  Kenneth Greisen \cite{Gzk}, Georgiy Zatsepin and Vadim Kuzmin\cite{gZK}, and describes interactions between high energy protons and \gls{cmb} photons.
	A proton traveling at a high Lorentz factor will see photons from the CMB approaching it as highly blue shifted and thus at high energy.
	A high energy photon interacting with a proton can create an unstable state known as $\Delta^{+}$ resonance, which quickly decays via two channels
	\begin{align}
		\gamma_\mathrm{CMB} + p &\rightarrow \Delta^{+} \rightarrow p + \pi^{0}\label{eq:GZKprod}\\
		\gamma_\mathrm{CMB} + p &\rightarrow \Delta^{+} \rightarrow n + \pi^{+}\,.\label{eq:GZKprod2}
	\end{align}
	The created pions then decay further via the known pion decay channels \cite{pdg}, with the neutral pions decaying into photons and the charged pions producing muons and neutrinos.
	
	The produced proton will continue on or interact with the CMB photons again if its energy allows it.
	The neutron travels for long distances given the large Lorentz boost, but ultimately decays into protons, electrons, and neutrinos. 
	The protons from this decay channel can equally interact with the CMB again, or be detected.
	During the processes described in \cref{eq:GZKprod} and \cref{eq:GZKprod2} the proton looses $\sim\SI{20}{\percent}$ of its energy \cite{gZK}, thus in each interaction with the CMB the high energy protons traveling through the universe will lose energy.
	This occurs until the proton looses energy to the point where the CMB photons are no longer blue shifted enough to have the energy to create a $\Delta^{+}$ resonance.
	This limit is reached at an energy of $\sim\SI{5e19}{\electronvolt}$ and is referred to as the \gls{gzk}.
	Smoking gun evidence for the GZK would be a strict cutoff at this threshold energy and a CR composition which is dominated by protons.
	If CRs above these energies are only protons, they should be very rare and can only come from sources sufficiently close enough to meet few CMB photons in transit.
	This distance is called \emph{GZK horizon}.
	Photons and neutrinos from the interactions and decays of particles in \cref{eq:GZKprod} and \cref{eq:GZKprod2} should also be observed at a similar rate as the proton primaries.
	Ultra-high energy photons and cosmogenic neutrinos have been searched for by many experiments \cite{icecube, philip-thesis, philipICRC}, however to date none have been found.
	Composition measurements also indicate a heavier composition at those energies instead of the lighter one required (see \cref{sec:composition}).

	\item[Photo-disintegration:] Another effect also considered to be part of the GZK limit as it concerns high energy particles seeing blue shifted  \gls{ebl} and CMB photons, is photo-disintegration.
	In contrast to direct interactions creating new particles, in this case the photon energy is high enough to exceed the binding energy of the nucleons in the CR nuclei, which then break apart.
	Each interaction in this process results in two or more CRs with lower energy and lighter composition, leading to a cutoff similar to the GZK case.
	However, the photo-disintegration cutoff would appear more like steps in the spectrum and composition at the energies where this effect occurs for each component of the CR flux.
\end{description}

In truth, it is likely that all three described effects contribute to the spectrum at the highest energies.
Figuring out exactly which of these effects dominates is however important to determine the sources and acceleration mechanisms of these extreme energy CRs.
Because of this, a measurement of the composition of CRs at these energies is key to determining which of these processes is occurring.
Unfortunately, because at these energies the flux is very low it is difficult to observe enough CRs using established composition measurement methods to infer primary mass.
This means that new methods, which are able to measure the composition of these rare CRs at high statistics, are desperately needed.
The AugerPrime upgrade and this thesis in particular aim to provide exactly one such methods.

\section{Detection of Cosmic Rays}
As mentioned in \cref{sec:SpectrumFeatures}, there are two different approaches to the detection of CRs, direct and indirect.
While direct measurements have the advantage of easily reconstructing information about a CR, such as its mass composition and charge, they must be space-borne experiments or very high up in the atmosphere, as the atmosphere absorbs CRs.
This means they cannot be very large, and because the flux sharply decreases with increasing energy, this makes them only feasible up to certain energies.
Thus, as energies increase to the TeV range, an alternative is needed.

\subsection{Extensive Air Showers}
While CRs themselves do not usually reach the surface of the Earth, they induce what are called \gls{eas} in the Earth's atmosphere, which can be measured by ground-based detectors.
An EAS can generally be described as a chain reaction which creates tens of billions of particles.
Through detection of these `secondary' CRs in this cascade of particles in the atmosphere, many of the characteristics of the CR primary can be inferred.
In order to do so however, one must understand how such an air shower develops in the atmosphere.
A schematic view of an EAS can be seen in \Cref{fig:airshower}.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/02_CosmicRaysAndExtensiveAirshowers/airshower.pdf}
	\caption[Extensive air shower development]{The development of an extensive air shower induced by a cosmic ray. The different types of cascades and components (electromagnetic, hadronic, muonic) are shown, indicated by the respective colors (blue, orange, green).}
	\label{fig:airshower}
\end{figure}
\noindent

The Earth's atmosphere is constantly exposed to an isotropic flux of CRs which then collide with the nuclei of the atmospheric constituents ($\mathrm{N_{2}}$, $\mathrm{O_{2}}$, Ar, \etc).
The point or depth of the CRs first interaction, $X_{0}$\footnote{Not to be confused with the radiation length $X_{0}$ (measured in \si{\gcm}) of a material, which represents the mean length to reduce the energy of an electron by a factor of $\frac{1}{e}$ in that specific material}, (see \cref{subsec:EASchar}) with an atmospheric nucleus depends strongly on its mass and energy.
In the first interaction the majority of particles produced are pions, and in smaller numbers, kaons.
The three types of pions, \ie $\pi^{\pm}$ and $\pi^{0},$ are produced in roughly equal amounts.
The neutral pions almost exclusively decay into pairs of photons, which in turn create electrons and positrons via pair-production on atmospheric nuclei.
These in turn produce more photons through bremsstrahlung causing a repeating alternation of the two processes.
This part of the shower is the main contribution to what is known as the \emph{electromagnetic component}.

The charged pions have a much longer lifetime than the neutral ones and therefore longer decay lengths.
If charged pions or the charged kaons decay before interacting, they produce muons which are very likely to survive all the way to the ground.
This interaction chain is referred to as the \emph{muonic component}.
If by chance the muon decays before reaching the ground it does so by producing electrons and thus contributes to the electromagnetic component.

Given their relatively long lifetimes, the high energy pions and kaons are likely to interact with other air nuclei before they decay and therefore produce more pions.
Additionally, fragments of the primary particle, as well as protons and neutrons that are produced in these interactions, also interact further producing more hadronic particles.
This chain is referred to as the \emph{hadronic component}.
All interactions in the hadronic component feed into both the electromagnetic and muonic component, slowly robbing it of energy \cite{boo:AstPhyGrup}.

As the EAS travels through the atmosphere, more and more particles are produced which share the primary energy.
These secondaries will eventually reach a point where their average energy is not enough to produce further particles in interaction, and therefore the particle production rate falls below the rate at which particles are absorbed in the atmosphere.
The atmospheric depth at which this turning point occurs is referred to as the depth of \emph{shower maximum}, \xmax{}, since the number of particles in the shower is highest there.
Due to the direct relation between number of particles and amount of emitted light, the shower is also the brightest at this point.
Lastly, and most importantly for this thesis, because the number of interaction lengths between $X_{0}$ and \xmax{} is dependent only on the primary energy, the location of \xmax{} can be used as a proxy for $X_{0}$, which in turn provides information on the primary mass.

\subsection{Characterizing an EAS}\label{subsec:EASchar}
A simple schematic of an EAS evolving in the air can be seen in \Cref{fig:shower_chars}.
In general, one of the most useful quantities with which to characterize a hadronic air shower is the slant depth $X$, which represents the amount of atmosphere the shower has penetrated up until a given point during its development.
For inclined showers, those with zenith angles up to \SI{60}{\degree}, it can be easily obtained through $X_{v}$, the vertical depth.
$X_{v}$ is calculated by integrating the density of the air from the top of the atmosphere to the height $h$ corresponding to the point of interest
\begin{align}
	X_{v}(h) = \int_{\infty}^{h}{\rho(h')\mathrm{d}h'}\,,
\end{align}
giving it a unit of \SI{}{\gram\per\centi\meter\squared}.
Then, by accounting for the zenith angle of the shower, the slant depth can be calculated as
\begin{align}
	X = \frac{X_{v}}{\cos{\theta}}\,.
\end{align}

The \emph{longitudinal development profile} describes how the number of shower particles, $N(X)$, changes throughout the development of the EAS, and is illustrated in \Cref{fig:shower_chars} by the orange curve above the particle cascade.
Also called the shower profile, it can be well parameterized by the Gaisser-Hillas function \cite{GaisserHillas}
\begin{align}
	N(X) = N_\mathrm{max} \left(\frac{X - X_{1}}{X_\mathrm{max} - X_{1}}\right)^{\frac{X_\mathrm{max} - X_{1}}{\lambda}}\exp{\left(\frac{X_\mathrm{max} - X_{1}}{\lambda}\right)}\, ,
\end{align}
where $N_\mathrm{max}$ is the maximum number of particles observed at \xmax{}.
The parameters $X_\mathrm{1}$ and $\lambda$ are fit parameters which depend on primary mass and energy. 
As mentioned earlier, \xmax{} is a variable which is strongly related to the composition of the primary CR and thus is very important to this work.
Because of this, it will be described in more detail in \cref{sec:FDAuger}.
The integral of this shower profile yields the total number of particles produced in the shower which is directly proportional to the calorimetric energy of the shower.

The most important geometric characteristic of an EAS is the \emph{shower axis}, which describes the vector the shower develops along from the point of first interaction through the atmosphere to the \emph{shower core} and is defined by its zenith, $\theta$, and azimuth, $\phi$, angle and shower core position.
The term shower core (see \Cref{fig:shower_chars}) is used to reference both the point at which the shower axis intersects the ground and the central part of the shower immediately around the shower axis where most of the hadronic component develops.
At any particular instant in time during its development, the shower would appear as a large, thin, dense disc of energetic particles called the \emph{shower front}.
The shower front is curved, as particles which travel away from the shower axis during development arrive at some observation level later in time, due to their longer path lengths.
This effect also means that the shower front is relatively thin close to the shower core and widens when moving away from it, as is illustrated in \Cref{fig:shower_chars}.

When a shower front intersects the ground it can be characterized by the time and density of arriving particles which is referred to as the \emph{shower footprint}.
Because the shower footprint is the primary observable for the ground arrays of CR observatories, it is the shower characteristic most often used to reconstruct and classify CR air showers.
The arrival time of the shower footprint at each point on the ground can be used as a means to reconstruct the shower geometry, in particular the shower axis and the curve of the shower front.
With the geometry available, the density of arriving particles can be correlated to their radial distance from the shower core on the ground to obtain the \gls{ldf}, which can be used to reconstruct the shower energy.
For this, typically the LDF is fit with an NKG-like \cite{NKG1, NKG2} function\footnote{The NKG (Nishimura-Kamata-Greisen) function was developed as a theoretical approach to describe the lateral distribution of electrons and muons.}
\begin{align}
	\rho_\mathrm{e}(r) = \frac{N_\mathrm{e}}{2\pi R^{2}_\mathrm{M}} C(s) \left(\frac{r}{R_\mathrm{M}}\right)^{(s-2)}\left(\frac{r}{R_\mathrm{M}} + 1\right)^{(s-4.5)}\,,
\end{align}
with parameters
\begin{align}
	s=\frac{3}{1+2X_\mathrm{max}/X}\,,\quad\text{and}\quad R_\mathrm{M}=0.0265X_{0}(Z+1.2)\,.
\end{align}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.85\textwidth]{figures/02_CosmicRaysAndExtensiveAirshowers/shower_chars.pdf}
	\caption[Extensive air shower characteristics]{Schematic depicting an EAS.}
	\label{fig:shower_chars}
\end{figure}
\noindent

The parameterized LDF is then zenith corrected and converted to the energy of the primary particle using either models or cross calibration from atmospheric measurements of the shower profile (for more details see \cref{chap:PAO}).
Of particular importance to this thesis is the fact that each of the three components of an EAS evolves differently, has different lateral particle density distributions, and therefore different shower footprints on the ground.
Since the relative fraction of particles in each component depends on the primary composition, if the footprints of the electromagnetic, muonic, and hadronic showers can be distinguished, the type of particle which started the EAS could be reconstructed by a ground array.

\section{The Composition of Ultra-High Energy Cosmic Rays}\label{sec:composition}
As mentioned previously in this chapter, the composition of CRs is very important to understand more about the sources of CRs and their acceleration mechanisms.
While at lower energies, with space-borne detectors, the composition of detected cosmic rays can be measured directly and is described in \cref{sec:SpectrumFeatures}, this is not possible anymore above a few TeV as CRs are detected indirectly via their EAS.
With indirect detection, composition measurements become model dependent, since the interaction processes taking place in an EAS must be simulated and therefore rely on different models, \eg \cite{EPOS-LHC, Sybill2.3c, QGSJET-II-04}.
This introduces uncertainties to measurements of the primary mass as these models are based on the extrapolation of accelerator measurements to the much higher energies of CRs.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/02_CosmicRaysAndExtensiveAirshowers/Kascade_and_Grande.png}
	\caption[KASCADE+KASCADE-Grande cosmic ray composition measurement]{CR spectrum as measured by KASCADE+KASCADE-Grande. The black squares indicate the measured all-particle spectrum, while the red circles and blue diamonds represent the heavier and lighter mass components, respectively. Taken from \cite{KascadeComposition}.}
	\label{fig:kascade_grande_composition}
\end{figure}
\noindent

One approach to indirect composition measurements is to infer the mass of primary CRs from the differences and ratios of the three shower components measured in the detected particle shower.
This method was leveraged by the \gls{kascade} experiment \cite{KascadeMethod}, which measured the electromagnetic and muonic components separately, as well as at its extension KASCADE-Grande, which measured all charged particles.
They used a combination of different detectors that are each dominantly sensitive to one of the shower components of an air shower to reconstruct the number of particles $N_\mathrm{e/\upgamma}$, $N_{\upmu}$, and $N_\mathrm{ch}$ from the different detector responses.
The ratios between the electromagnetic and muonic component $N_\mathrm{e/\upgamma}/N_{\upmu}$ (KASCADE) and the ratio between the charged and muonic component $N_\mathrm{ch}/N_{\upmu}$ (KASCADE-Grande) are sensitive to different primary masses.
This is because primaries with the same $N_\mathrm{e/\upgamma}$ or $N_\mathrm{ch}$, will have different $N_{\upmu}$.
Also shower-to-shower fluctuations for events of the same primary mass tend to fluctuate similarly in $N_\mathrm{ch}$ or $N_\mathrm{e/\upgamma}$ and thus primary composition is reconstructable on an event-by-event basis.
A parameter $k$ is calculated for different zenith intervals as
\begin{align}
	k = \frac{\log_{10}(N_\mathrm{ch}/N_{\upmu})_\mathrm{event}-\log_{10}(N_\mathrm{ch}/N_{\upmu})_\mathrm{H}}{\log_{10}(N_\mathrm{ch}/N_{\upmu})_\text{Fe}-\log_{10}(N_\mathrm{ch}/N_{\upmu})_\mathrm{H}}\, ,
\end{align}
where $\log_{10}(N_\mathrm{ch}/N_{\upmu})_\mathrm{H}$ and $\log_{10}(N_\mathrm{ch}/N_{\upmu})_\mathrm{Fe}$ are calculated using Monte Carlo simulation and therefore depend on the hadronic interaction model chosen.
Per definition, $k$ is centered around one if the primary is iron and centered around zero if the primary is a proton.
Each event can be marked as lighter (H+He) or heavier (C+Si+Fe) in composition, based on the value of $k$ for that event.

Composition results for combined KASCADE and KASCADE-Grande data using this method can be seen for two different hadronic interaction models in \Cref{fig:kascade_grande_composition}.
The results verify the first knee structure mentioned in \cref{sec:spectrum} at \SI{3e15}{\electronvolt} for the lighter elements, followed by increasing dominance of heavier elements and a second knee at \SI{e17}{\electronvolt} for the heavier elements.
Additionally, an increase in lighter elements after the second knee is visible \cite{KascadeComposition}.

Another approach for measuring the composition of CRs is to use the longitudinal parameter \xmax{} mentioned in \cref{subsec:EASchar}.
This has been most notably used by the Fly's Eye experiment, the Pierre Auger Observatory, and the Telescope Array \cite{FlysEye, AugerCompositionI, AugerCompositionII, TA}.
Shower-to-shower fluctuations in the first few hadronic processes of the cascade mean that the primaries mass can only be inferred from the distribution of shower maxima, instead of on an event-by-event basis.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.8\textwidth]{figures/02_CosmicRaysAndExtensiveAirshowers/MeanXmax_and_RMS.pdf}
	\caption[Pierre Auger Observatory $\braket{X_\mathrm{max}}$ and $\sigma^{2}(X_\mathrm{max})$]{Pierre Auger Observatory measurements of  $\braket{X_\mathrm{max}}$ (left) and $\sigma^{2}(X_\mathrm{max})$ (right), showing \xmax{} getting deeper towards the ankle indicating light composition and then shallow beyond the ankle indicating heavier composition. Taken from \cite{recentXmax}.}
	\label{fig:auger_xmax}
\end{figure}
\noindent

Assuming a mixed composition, this distribution is a superposition of all the individual distributions $f_{i}(X_\mathrm{max})$ created by the different primary particles $i$ with mass $A_{i}$ multiplied by the fraction of the flux represented by that primary type $p_{i}$:
\begin{align}
	f(X_\mathrm{max}) =\sum_{i} p_{i} f_{i}(X_\mathrm{max})
\end{align}
This first two moments of this distribution, \ie its mean $\braket{X_\mathrm{max}}$ and variance $\sigma^{2}(X_\mathrm{max})$, respectively, are linearly proportional to the logarithm of the primaries mass $\ln(A)$ \cite{AugerCompositionI} and its variance.
A detailed review on the proportionality factors between $\ln(A)$ and $\braket{X_\mathrm{max}}$ and $\sigma^{2}(X_\mathrm{max})$ is given in \cite{Xmax_lnA}.

Recent results on $\braket{X_\mathrm{max}}$ and $\sigma^{2}(X_\mathrm{max})$ from the Pierre Auger Observatory \cite{recentXmax} can be seen in \Cref{fig:auger_xmax}, as well as the inferred logarithm of the primary mass depending on the hadronic interaction models used in \Cref{fig:auger_lnA}. 
In accordance with the KASCADE results, the composition gets lighter towards the ankle, where it clearly has a significant proton fraction.
As energies increase beyond the ankle, the composition gets increasingly heavier again.
A limiting factor in these results and others at the same energies, and as a result in answering the earlier mentioned open questions, is a lack of statistics at the highest energies.
The necessity of increasing the high energy statistics to answer these open questions is one of the driving factors behind the upgrade of the Pierre Auger Observatory, AugerPrime, and is the focus of this thesis.
The goals of AugerPrime are to shed light on the mass composition and source of the flux suppression of UHECRs, as well as better understanding the hadronic interactions in EAS at these energies well beyond those available at collider experiments.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.85\textwidth]{figures/02_CosmicRaysAndExtensiveAirshowers/lnAMoments.pdf}
	\caption[Pierre Auger Observatory $\ln(A)$ moments]{Measurements of $\ln(A)$ moments performed at the Pierre Auger Observatory also indicating lighter composition around the ankle and heavier beyond. Here, it becomes apparent that the results are very model-dependent. In particular QGSJETII-04 appears non-physical above \SI{1}{\exa\electronvolt} due to its purer than pure composition. Taken from \cite{recentXmax}.}
	\label{fig:auger_lnA}
\end{figure}
\noindent
