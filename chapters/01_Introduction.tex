\chapter[INTRODUCTION: ASTROPARTICLE PHYSICS AT THE HIGHEST ENERGIES]{Introduction: Astroparticle Physics at the Highest Energies}
\epigraph{\itshape\myopeningquote The molecules of your body are the same molecules that make up this station, and the nebula outside, that burn inside the stars themselves. We are star stuff. We are the universe made manifest, trying to figure itself out.\myclosingquote}{--- Ambassador Delenn, \textit{Babylon 5}}

The universe has always been a source of fascination and awe for humans.
Astroparticle physics is a branch of physics that developed out of this strong interest, and has been a major field of study for more than a hundred years.
Despite this long history, there are still many open question across the field.
A central one, that this thesis hopes to help to address, is ``What are \glspl{uhecr}, the highest energy particles ever observed, composed of?''
This is a particularly important question, as only with a good grasp on UHECR composition it is possible to shed light on their acceleration mechanisms or back-track them from Earth through the magnetic fields to their sources.

The most prominent experiment for studying \glspl{cr} at the highest energies is the Pierre Auger Observatory in Argentina.
It is particularly powerful as it has both a Surface Detector which is always watching for the particles that reach the ground from UHECR interactions in the atmosphere, and a Fluorescence Detector which can directly observe those interactions as the shower develops in the atmosphere.
Because it directly observes the shower evolution, the Fluorescence Detector of the Observatory currently has the highest shower-by-shower primary particle mass resolution.
However, because it can only measure during very dark and clear nights, it has a rather low duty cycle which results in low statistics at the highest energies.
To address this problem, the upgrade of the Observatory, AugerPrime, is being built to improve the primary mass resolution of the Surface Detector, and aims to do so by adding surface scintillators on top of the existing water Cherenkov detectors.
The combination of these two detector types provides enhanced mass sensitivity, because the two detectors have differing responses to the different components in extensive air showers and the relative ratio of these components is a strong indicator of primary mass.

In this thesis, the performance of prototype upgraded stations is studied over the first two years of data taking.
The expertise gained from the analyses carried out on this data is then used to develop a deep learning approach to reconstructing the depth of shower maximum, \xmax{}, which is an indicator of primary mass, based on AugerPrime simulations.
Two neural networks are trained and compared, one with and one without scintillator detector information, in order to gauge the prospects of enhancing the shower-by-shower composition sensitivity of the Surface Detector with AugerPrime.

The thesis is split into eight chapters.
After this introduction, \cref{chap:UHECRs} gives an overview of cosmic ray physics at the highest energies.
Briefly, to frame the importance of UHECR composition, the energy spectrum of cosmic rays is discussed with a description of its features and their origin.
Special focus is given to the composition around the so-called ankle of the spectrum and beyond, as this is the energy range studied in this thesis.
Extensive air showers are introduced as they are the phenomenon used by the Observatory to indirectly measure cosmic rays.
This includes an overview of the different shower components as well as the main characteristics used to define an extensive air shower.

Simulated measurements of UHECRs made by the Pierre Auger Observatory serve as the basis of this research and as a result a good understanding of the different components used for the detection of UHECRs at the Observatory is needed.
Because of this, \cref{chap:PAO} gives an overview of the instrumentation and software used at the Observatory.
Particular attention is given to Surface Detector data acquisition, calibration, and event reconstruction, which are described in detail.
The upgrade AugerPrime, which is currently underway, is also introduced and all components are described, with a focus on the new scintillator detectors and the electronics upgrade, which are the core components needed to enhance the composition resolution of the Surface Detector.

A first set of 12 upgraded Surface Detector stations was deployed around the time the research of this thesis began.
To provide a firm base for the later analysis of Surface Detector stations in simulation, in \cref{chap:EAData} the data from these prototype stations is analyzed to profile their performance.
This was a crucial step for both selecting possible parameters for the deep neural network to train on, and for understanding the performance of the trained network.
As such, in-depth studies were performed to evaluate the timing accuracy of the detectors, the baseline stability of the signal pulses, and their detection efficiency with  respect to the zenith angle.

To serve as a background for the later chapters, an overview of artificial neural networks as a method in general is provided in \cref{chap:DL}.
This includes a description of their basic components and the mechanisms with which they learn.
As they serve as the backbone of the network architecture developed in this thesis, Cartesian and hexagonal convolution methods are described in detail, along with a quick overview of the other general layer types needed in this work. 

\Cref{chap:PrePro} describes the detector simulation process and data pre-processing steps.
The parameters used to train the neural network are selected.
This includes the two detector traces of the upgraded Surface Detector stations, the reconstructed energy and zenith angle, and the arrival times of the shower in each station.
Since the input data for the neural network requires it to have certain array shapes, the data is slightly augmented before being fed into the neural network for training.
Additionally, a method developed to improve the performance of the network, a rotation of the mapping for each event, is introduced.
This serves as a method of increasing training speed by reducing the complexity of the network.

The network architecture developed for this thesis is presented in \cref{chap:THEANALYSIS}.
It is then used to train two models, one with and one without the data from the new scintillator detectors, which are then used to predict the \xmax{} of simulated showers.
Two additional methods of data augmentation are introduced and applied during training.
One serves as a means of bootstrapping the training data by randomly simulating non-functioning Surface Detector stations, while the other method aims to help the network give more attention to under-represented \xmax{} values.
The two models trained from this are then evaluated based on their composition dependent reconstruction biases \xmaxmu{} and their overall reconstruction resolution \xmaxsigma{}.
After correcting for zenith dependent biases, the two models are compared and a definite improvement in composition sensitivity through the addition of the new detector is found.
This is also quantified by investigating the merit factors of both models, which show that the model trained with both detector traces has a greater ability to distinguish between proton and iron induced air showers, across all energies.

The results found are finally summarized in \cref{chap:Conclusion}.
Here, both models are compared to another deep learning approach developed within the collaboration, which uses only water Cherenkov detector information.
It is also evident there that the additional scintillator information increases the composition sensitivity of the Surface Detectors further.
A short outlook on the future prospects of the research presented in this thesis is given and a few further improvements to the pre-processing and the network architecture are proposed.


