\chapter[COMPARISON OF TWO AUGERPRIME SIMULATION SETS]{Comparison of two AugerPrime Simulation Sets}\label{app:offline-comp}
In an earlier version of the research presented in this thesis, a different set of simulations was used.
The earlier set of simulations was made for both iron and proton primaries locally in Wuppertal (here also referred to as BUW) with the most recent version of \Offline{} available at the time, with the tag \texttt{v3r99p1}.
However, higher statistics and additional primaries were needed to train the neural networks presented here.
The Collaborations MC task was more well suited to produce such an AugerPrime simulation library.

\begin{figure}[!htb]
	\centering
	\subfloat[][$E_\text{MC}$]{\includegraphics[width=0.48\textwidth]{figures/A_Appendix/MCEnergy_dist_comp.pdf}}
	\subfloat[][\ESD{}]{\includegraphics[width=0.48\textwidth]{figures/A_Appendix/Energy_dist_comp.pdf}}\\
	\subfloat[][\thetaSD{}]{\includegraphics[width=0.48\textwidth]{figures/A_Appendix/Zenith_dist_comp.pdf}}
	\subfloat[][$\phi_\text{SD,rec}$]{\includegraphics[width=0.48\textwidth]{figures/A_Appendix/Azimuth_dist_comp.pdf}}
	\caption[Relative error on \ESD{} reconstruction]{Distribution of event-level parameters $E_\text{MC}$, \ESD{}, \thetaSD{}, and $\phi_\text{SD,rec}$.}
	\label{fig:offline_comp_dists}
\end{figure}
\noindent

There had been a lot of changes to the AugerPrime simulation code since making the first small production in Wuppertal and updates in the trigger simulation were in progress.
As a result a study was needed to decide between making a larger scale simulation library with the \texttt{v3r99p1} version of \Offline{} or a newer one.
The newer version to compare the Wuppertal simulations to was chosen to be the revision \texttt{r33552}, which contained a lot of bug fixes in regards to the GEANT4 modules.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.6\textwidth]{figures/A_Appendix/Energy_diff_comp.pdf}
	\caption[Relative error on \ESD{} reconstruction]{Relative \ESD{} reconstruction error.}
	\label{fig:rel_E_rec_error}
\end{figure}
\noindent

Two small simulations set were subsequently compared, one with \Offline{} \texttt{v3r99p1} and on with \Offline{} \texttt{r33552}, both using proton as a primary and EPOS-LHC as the hadronic interaction model.
The \texttt{v3r99p1} simulations were made with CORSIKA simulations also made in Wuppertal, while the revision \texttt{r33552} simulations are made with the Napoli/Praha library.
All simulation are in the energy range $E_\text{MC}\in[\SI{19.0}{\SIlgE}, \SI{19.5}{\SIlgE}]$.
As can be seen from \Cref{fig:offline_comp_dists}, the most important event-level distributions, \ie $E_\text{MC}$, \ESD{}, \thetaSD{}, and $\phi_\text{SD,rec}$ all look comparable. 
Both simulations sets where produced with and energy spectrum of $E^{-1}$, but results look better for \texttt{r33552}.
Similarly both simulation sets were produced flat in azimuth, but shown slightly better performance for \texttt{r33552}.
This is however, very likely due to the underlying CORSIKA simulations and not necessarily an effect of differences in the two \Offline{} versions.

An important aspect is that the relative error on \ESD{}, shown in \Cref{fig:rel_E_rec_error}, is of the same quality for both versions of \Offline{}, as this is an indicator that the SD energy reconstruction is working properly.
At the time this study was carried out, the network was still trained with all stations in an event, \ie all stations with non-zero total signal, and not just candidate stations.
The average total signal in a station over all events is shown in \Cref{fig:average_signals}.
The station color indicates the strength of the signal, while the size of the circle is scaled by frequency of “appearance” of that station in the event set.
This is done separately for SSD and WCD in order to detect potential differences.
While the maximum average total signal in the WCDs is comparable, the maximum average total signal in the SSD is higher in the \texttt{r33552} simulations.
This is likely explainable by the different \Offline{} versions, due to a change in the MIP definition after \texttt{v3r99p1}, changing the magnitude of the signals.
Overall, the number of stations appearing in an event looks to be higher in the  \texttt{v3r99p1} set.
This was especially unexpected in the WCD and could indicate differences in the trigger simulations.

To investigate this further the station counts in the SSD than in the WCD are studied in \Cref{fig:station_counts}.
In both cases, the station count is higher for \texttt{v3r99p1}.
As an additional cross-check a single helium event from the Napoli/Praha library is simulated with both Offline versions.
The station count for this event also differs between the two Offline versions, an indication that the different underlying CORSIKA simulations in the proton simulations is not the reason for the differing station counts.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=\textwidth]{figures/A_Appendix/Signals_testnotclipped_axial.pdf}
	\caption[Average station signals and frequency of station appearance]{Each stations average total signal over all events is calculated and indicated by the color scale. The size of a station represents its average frequency of appearance, a measure of how often a station participates in events.}
	\label{fig:average_signals}
\end{figure}
\noindent

The proton \texttt{v3r99p1} simulations are also compared to a small set of helium simulations from the Napoli/Praha library processed with \texttt{v3r99p1} in Wuppertal.
The results of this are shown in \Cref{fig:station_counts} as well and are comparable, with the small offset likely an effect due to the difference in primary particle.
\begin{figure}[!htb]
	\centering
	\subfloat[][SSD Counts]{\includegraphics[width=0.48\textwidth]{figures/A_Appendix/SSD_Stations_Counts.pdf}}
	\subfloat[][WCD Counts]{\includegraphics[width=0.48\textwidth]{figures/A_Appendix/WCD_Stations_Counts.pdf}}\\
	\subfloat[][BUW Counts]{\includegraphics[width=0.48\textwidth]{figures/A_Appendix/Stations_Counts_BUW.pdf}}
	\caption[Average station counts]{The count of non-zero stations of all events. The histograms are normalized for comparison. The SSD and WCD counts are studied separately and differ, as a non-zero WCD station can potentially not have a signal in the SSD. The vertical lines in the top row show the respective station counts of the helium event simulated from the Napoli/Praha library with both \Offline{} versions. The bottom plot compared proton and helium events with \texttt{v3r99p1} but different underlying CORSIKA simulations.}
	\label{fig:station_counts}
\end{figure}
\noindent

As a last cross-check the total signals in both the SSD and the WCD are studied, focusing on very low signals, see \Cref{fig:low_signals}.
Overall, there are more stations with very low signal in both SSD and WCD for \Offline{} \texttt{v3r99p1}, which is probably a reason for the higher station counts.
As a result of the studies presented here, Offline revision \texttt{r33552} was chosen to make a first AugerPrime simulation library and this library is the one used for the models trained and presented in this thesis.
This decision was made, because the results from \texttt{r33552} are more comparable with what SD simulations of non-upgraded stations look like in regards to the WCD.
\begin{figure}[!htb]
	\centering
	\subfloat[][SSD Counts]{\includegraphics[width=0.48\textwidth]{figures/A_Appendix/SSD_total_signals.pdf}}
	\subfloat[][WCD Counts]{\includegraphics[width=0.48\textwidth]{figures/A_Appendix/WCD_total_signals.pdf}}
	\caption[Low total signals]{Very low total signals for SSD (<60) and WCD (<10) compared between the two \Offline{} versions.}
	\label{fig:low_signals}
\end{figure}
\noindent
