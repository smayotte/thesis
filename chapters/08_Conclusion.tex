\chapter[CONCLUSION AND FUTURE PROSPECTS]{Conclusion and Future Prospects}\label{chap:Conclusion}
\epigraph{\itshape\myopeningquote It isn't all over; everything has not been invented; the human adventure is just beginning.\myclosingquote}{--- Eugene Wesley "Gene" Roddenberry}

In this thesis, the composition sensitivity of the AugerPrime upgrade of the Pierre Auger Observatory was studied.
As a result, it was found that the resolution on the depth of shower maximum, \xmax{}, and the event-by-event accuracy of its reconstruction is definitively improved through the addition of the SSD.
More impressively, the measurement can be made bias-free, both with respect to mass and zenith angle, once an energy greater than \SI{18.8}{\SIlgE} is reached.

In \cref{chap:EAData}, early research of data obtained from prototype detectors in the AugerPrime Engineering Array is shown.
These analyses were first pursued to monitor and improve the data quality throughout the development of the upgraded electronics.
An especially important step, was the verification that the updated GPS modules are able to deliver the expected detector timing accuracy of $\sim\SI{9.4}{\nano\second}$ in the \SI{40}{MHz} compatibility mode.
This was critical, as a good timing resolution is essential for proper reconstruction of the shower geometry.
This also indicates that when the \SI{120}{MHz} sampling rate is implemented, the goal timing resolution of \SIrange[range-units=single, range-phrase=--]{5}{6}{\nano\second} will be reached.
However, these analyses also proved to be crucial to the development of the DNN.
This is particularly true in the identification of parameters, besides the two different detector responses, which should be added in order to increase the prediction power of the neural network.
Also, the understanding gained through these studies greatly informed on the causes and nature of different DNN behaviors observed throughout development and analysis.

Detector simulations of the fully upgraded array were then used to gauge the degree to which the AugerPrime hardware upgrade has the capability to improve composition sensitivity.
The simulated data sets generated for this are presented in \cref{chap:PrePro}.
From these, a selection of input parameters was made with the goal of using only the two detector responses, $S(t)_\mathrm{SSD}$ and $S(t)_\mathrm{WCD}$, and high-level observables that can be accurately reconstructed by standard reconstruction methods.
For this, the SD reconstructed energy, \ESD{}, the arrival time at the individual stations $t_{i}$, and the SD reconstructed zenith angle, \thetaSD{} were chosen.
The data was then adjusted and augmented to make it ready for processing with a DNN.
This pre-processing includes down-scaling observables to smaller numbers, and also a novel method of rotating the mapping of the stations, used in order to stabilize the network and improve the training speed.
This mapping rotation results in the active stations of an event always being at a similar position within the $9\times9$ grid.
This commonality between events means each node on average sees a similar part of the shower which results in the network learning faster.
Also with this method the nodes necessary for training are reduced and thus the time needed for the network to converge decreases.

With these parameters chosen, the network architecture was developed, taking advantage of both Cartesian and hexagonal convolutions.
The top half of the architecture is dedicated to comparing the two differing detector responses for each station individually via convolutions between the SSD and WCD traces.
The lower half of the network then uses the features extracted from each station and combines them with the zenith, energy, and arrival time, to extract temporal and spacial features stemming from relations between the different stations.
This information is then used to make a final prediction of \xmax{} for each event.
This network architecture was then used to train two models, one with both the SSD and WCD detector responses, \DNNSSD{}, and one with only WCD data, \DNNWCD{}.
In the network trained with both detector responses, an energy-dependent zenith dependence is found, likely due to the zenith dependent efficiency of the SSD.
However, because the zenith dependence is the same for all primary types in \DNNSSD{}, this zenith dependence can be corrected for.
The WCD-only network shows no zenith dependence on average, but instead shows differing zenith dependencies for each primary type meaning they can not be fully corrected for.
Nonetheless, the same two-dimensional parameterization method is applied to both networks and the resulting reconstruction biases \delxmaxmu{} and reconstruction resolutions \delxmaxsigma{} are compared.

After all analysis steps, it was found that the inclusion of the SSD trace information in the training of the model results in a consistently better resolution across all energies of around \SI{1.2}{\gcm}.
Impressively, at energies below \SI{18.9}{\SIlgE} the composition dependent bias was found to be as much as \SI{8}{\gcm} smaller with \DNNSSD{} than with \DNNWCD{}.
This holds true also for the highest energies where the bias gap remains for \DNNWCD{} but mostly vanishes for \DNNSSD{}.
In fact \DNNSSD{} here tends to over-predict the \xmax{} values of protons in comparison to the other primaries, resulting in a negative bias gap.
This is an indication that the network is better at identifying proton primaries than is possible with \xmax{} alone.
From these results, it is evident that the model trained with SSD and WCD trace information out-performs the WCD-only model in every regard.

Another deep learning effort within the Pierre Auger Collaboration, called \emph{AixNet}, using only the WCD, has shown similar results \cite{Aachen} as those obtained in this thesis.
The resolutions obtained with \DNNSSD{} and with AixNet are very similar.
However, the composition dependent bias below \SI{18.9}{\SIlgE} is much smaller with the \DNNSSD{} model developed in this thesis.

The merit factors for the models developed here and for AixNet are shown in \Cref{fig:MoFosComp}.
Since test set predictions made with AixNet are available within the collaboration, to ensure one-to-one comparability, the AixNet merit factor was also calculated here for each energy bin, with the same formulas for the moments used in this thesis, which were taken from \cite{GAP-2009-078}.
While the underlying detector simulations are not the same, the differences are primarily in the addition of the SSD and both simulations sets use the same MC library and hadronic interaction model (EPOS-LHC) and are therefore directly comparable.
From this comparison it seems that the results of both WCD-only models are similar below \SI{19.2}{\SIlgE}, with AixNet eventually performing better at higher energies.
From \Cref{fig:MoFosComp} it is also clearly evident that the additional information contained in the SSD traces results in a better composition sensitivity at all energies.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/08_Summary/mf_withAixNet.pdf}
	\caption[Merit factor comparison of \DNNSSD{}, \DNNWCD{}, and AixNet]{Comparison of the merit factor calculated for \DNNSSD{} (up-triangle, blue), \DNNWCD{} (down-triangle, orange), and AixNet (square, red) \vs. energy. The average MC merit factor is indicated by the dashed line (gray). It is clear that \DNNSSD{} overall has the best proton-iron separation, due to the addition of the SSD trace information.}
	\label{fig:MoFosComp}
\end{figure}
\noindent

There is also room to improve as there is still a number of possible tweaks to the pre-processing and network architecture that could improve performance, but could not be carried out in the time frame of this thesis.
In pre-processing, further augmentation of the input data is possible.
In its current form, both the signal traces of the WCD and the SSD, as well as the arrival times, can reach very large values, which are not ideal for processing by neural networks.
Methods to scale these three parameters down should be implemented in future studies of this kind and are currently being pursued within the collaboration.

Regarding the network architecture, the bi-directional \emph{long short-term memory} (LSTM) layers used for AixNet, likely could further improve the performance of \DNNSSD{}.
This is because they are well-suited to time series data, as they have the ability to form better connections between past and current time steps than the method used here and therefore could be used to replace the 3D Cartesian convolutions in the top half of the network architecture.
Even without this change, it is expected that further tweaks in the network architecture would decrease the remaining composition dependent biases, and, if desired, the over-prediction of \xmax{} for proton primaries above \SI{18.9}{\SIlgE}.
One prominent means of increasing performance would be to further improve the detector simulations obtained with \Offline{}, a work which is currently underway.
As mentioned in \cref{chap:PrePro}, these are still in preliminary form and there is still work needed to correct the implementation of the trigger simulations.
Once this is completed, the studies presented here could potentially be extended to energies below $\SI{18.5}{\SIlgE}$.

Despite these opportunities for improvement, the merit factor results for \DNNSSD{} have shown that the theoretically optimal distinction between proton and iron \xmax{} distributions is already reached above \SI{18.9}{\SIlgE}.
Because \xmax{} is inherently random to some degree, the networks are trying to predict a random variable.
This likely is a cause for the seemingly hard resolution limit of \SI{25}{\gcm} seen both here and in AixNet.
This means it is likely that further distinction between masses would only be possible with a transition from a regression task, where the network learns to predict the underlying distribution of \xmax{}, to a classification task that directly predicts the mass or particle type.
With all of this in hand, it is clear that the studies presented in this thesis show that the goal of achieving high-accuracy composition measurements on an event-by-event basis is possible with AugerPrime.