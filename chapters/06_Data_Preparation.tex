\chapter[SIMULATION SETTINGS AND PRE-PROCESSING]{Simulation Settings and Pre-processing}\label{chap:PrePro}
\epigraph{\itshape\myopeningquote None of this is real. It is a simulation. We are still on the holodeck.\myclosingquote}{--- Lt. Commander Data, \textit{Star Trek: The Next Generation}}

A crucial step before applying a neural network to any data is to prepare it for read-in by the network.
This step is often called \emph{pre-processing} and can refer to the common processes of data augmentation, such as normalizing or re-sizing of the data, or zero padding it.
Prior to this the parameters to train the network with have to be chosen.
In this thesis, this mainly concerns selecting the parameters of SD events which are expected to contain the information needed to achieve the goal of reconstructing \xmax{}.

Additionally, to train a neural network the label, in this case the  \xmax{} value of an event, has to be known for the network to learn from.
Because higher statistics are needed than the FD can provide \gls{mc} simulations are used.
While the code to generate AugerPrime detector simulations is now available in \Offline{}, it is not yet in a final state regarding the simulation of the trigger.
This was in part identified in a study, shown in \cref{app:offline-comp}, carried out during this research in order to choose which revision of the \Offline{} code should be used to produce the detector simulations needed to train the network.
Even though they are still preliminary, the AugerPrime simulations can be used to investigate the expected mass composition sensitivity of AugerPrime.

\section{CORSIKA Simulation Setup}
Before generating AugerPrime detector simulations, the EAS in the atmosphere needs to be simulated separately using the simulation software CORSIKA \cite{CORSIKA}.
Within the Pierre Auger Collaboration, a library of CORSIKA shower simulations is available, commonly referred to as the \emph{Napoli/Praha library}.
In this thesis, the simulations use EPOS-LHC \cite{EPOS-LHC} as the hadronic interaction model.
The available files cover a zenith range of $\theta\in[\SI{0}{\degree}, \SI{65}{\degree}]$, which is simulated so that the distribution is flat in $\cos^2(\theta)$.
The simulations are distributed over an energy range of $E\in[\SI[parse-numbers=false]{10^{18}}{\electronvolt}, \SI[parse-numbers=false]{10^{20.2}}{\electronvolt}]$, with $E^{-1}$ as the energy spectrum.
This is done for 1000 CORSIKA files per \SI{0.1}{\SIlgE} bin, resulting in 22000 CORSIKA files for each of the four simulated primaries: proton, helium, oxygen and iron.
In this thesis, unfortunately only the files with an MC energy above \SI{18.5}{\SIlgE} are used.
This is because in the subsequent AugerPrime detector simulations with \Offline{}, earlier mentioned short-comings in the trigger simulations especially affect very low energies, where the SD trigger is not fully efficient.

\section{AugerPrime Simulation and Reconstruction}
To create full detector AugerPrime simulations, the \Offline{} Standard Application \texttt{SdSimulationReconstructionUpgrade} is used with standard settings.
In order to increase statistics, an EAS simulated with CORSIKA can be processed with \Offline{} multiple times.
In the first step of creating \Offline{} simulations, a CORSIKA shower core is placed at random point within the SD array.
From there, using the CORSIKA generated particle profile at ground the detector response to such a shower can be simulated.
The CORSIKA shower can then be ``reused'' by placing it at a different spot within the array.
This is possible because the detector simulations introduce a large amount of randomness by simulating random noise particles or through the differing placement of the shower with respect to the SD stations.

For this thesis, each CORSIKA shower is reused 10 times to create sufficient statistics to train a neural network and this can be done without worrying about overuse of the CORSIKA library.
In the next step of the simulation, the \emph{CachedShowerRegenerator} is used to translate the CORSIKA shower to a particle flux on each SD Station.
The GEANT4 \cite{GEANT4} package is then used to simulate particles and track them through the SSD and WCD.
As the particles pass through the detectors, photons from scintillation or water Cherenkov are generated and then further tracked.
This is done until the photons either decay or reach a PMT, in which case they are recorded.
The number of photo-electrons as a function of time is used as input for the next module which simulates the PMT and electronics responses.
The simulated signal traces are then created based on parameterizations from extensive studies of the PMT responses to single photons and noise levels.
After these simulation steps, the signals are subjected to the same procedures as real data, \ie the calibration and trigger criteria described in \cref{sec:SDTrigger}.
Finally, the event is reconstructed following the procedure mentioned in \cref{chap:EAData} and both the reconstructed values and the MC truth are stored in ADSTs.

To remove low quality events and failed reconstructions, the simulated ADST files can be further processed with an \Offline{} tool called \texttt{selectEvents} which applies quality cuts and merges all files for easier data processing.
The \texttt{SDcut} file containing the cuts applied for this thesis can be found in \cref{app:selEvents}.
The most important quality cut applied, is the requirement to keep only events that pass the 6T5 trigger (for a definition see \cref{sec:SDTrigger}).
This ensures the highest data quality.
Additionally, the reconstructed zenith range is restricted to $\theta\in[\SI{0}{\degree}, \SI{60}{\degree}]$, since it has been shown in \cref{chap:EAData} that the SSD is not efficient above \SI{60}{\degree}.
Events with reconstructed energies below \SI[parse-numbers=false]{10^{18.5}}{\electronvolt} are also removed, since the MC energy range has a lower limit of \SI[parse-numbers=false]{10^{18.5}}{\electronvolt}.
As a result events with an MC energy lower than \SI[parse-numbers=false]{10^{18.5}}{\electronvolt} are not present and this lowest bin of energies between \SI[parse-numbers=false]{10^{18.5}}{\electronvolt} and \SI[parse-numbers=false]{10^{18.6}}{\electronvolt} is affected by edge effects.
Events with a reconstructed energy lower than \SI[parse-numbers=false]{10^{18.5}}{\electronvolt} are lost, while no events from over-reconstructions are gained from a lower bin.
This might affect the composition reconstruction of the NN trained in this thesis and as a result, there might be unaccounted for biases in the lowest energy bin.
To fix this in the future, MC simulations from the lower bin of \SI[parse-numbers=false]{10^{18.4}}{\electronvolt} should also be used and the cut on reconstructed energies should be applied afterwards.
However, this error was identified too late in this thesis to correct for it in the results presented here.
No upper limit is set on the reconstructed energies, as there the reconstruction is generally very robust due to high station counts.

\section{Pre-processing Detector Simulations with Python}
The merged ADST files are read-in using the Python tool PyIK mentioned in \cref{sec:pyik}, to extract the parameters needed to train the neural network.
The subsequent pre-processing and data augmentation algorithms are performed with Python as well.

\subsection{Parameter Selections and Read-out}
To reconstruct \xmax{}, primarily the SSD and WCD detector responses will be used.
Additionally, a few high-level parameters, which can be obtained with high precision from the SD reconstruction, are used to focus the network on \xmax{}.
Following this plan, the following parameters are selected as input for the DNN:
\begin{itemize}
	\item the calibrated signal traces $S(t)_\mathrm{SSD}$ and $S(t)_\mathrm{WCD}$ (mean over all three WCD PMT traces)
	\item the arrival time at the individual stations $t_{i}$
	\item the SD reconstructed energy \ESD{}
	\item the SD reconstructed zenith angle \thetaSD{}
	\item the MC depth of shower maximum $X_{\mathrm{max,MC}}$ as the label
\end{itemize}
The two detector traces are the most integral part in this study.
The goal is to reconstruct the shower maximum on an event-by-event basis primarily based on the differences between the signal traces recorded by the two detector types and to study the improvement in composition sensitivity gained with the addition of the SSD.
The arrival time, zenith, and energy are added, as they are the most descriptive high-level parameters in an event and thus are likely useful to determine the \xmax{} of an event.
For training and predicting, only SD reconstructed energies and zeniths are used, since the MC information is not available in real data.
The distributions of the event-level parameters \ESD{}, \xmaxmc{}, and \thetaSD{} can be seen in \Cref{fig:dataset_dists}.
\ESD{} and \thetaSD{} are read out and used as is.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.49\textwidth]{figures/06_Data_Preparation/Energy_dist.pdf}
	\includegraphics[width=0.49\textwidth]{figures/06_Data_Preparation/Zenith-2_dist.pdf}\\
	\includegraphics[width=0.49\textwidth]{figures/06_Data_Preparation/Xmax-true_dist.pdf}
	\includegraphics[width=0.49\textwidth]{figures/06_Data_Preparation/Xmax-true-corr_dist.pdf}
	\caption[Energy, Zenith, and \xmax{} distributions of full data-set]{Full data set distributions. Top left: The \ESD{} distribution is flat in $E^{-1}$. The earlier mentioned effects of bin migration and edge effects are visible in the highest and lowest bins. Top right: Zenith angle distribution where \thetaSD{} is plotted as a function of $\cos^2(\theta_\text{SD,rec})$ to show the flatness. Bottom: MC \xmax{} distribution to be used as the truth value \xmaxmc{} before (left) and after (right) accounting for the elongation rate.}
	\label{fig:dataset_dists}
\end{figure}
\noindent

\subsection{Data Augmentation and Elongation Rate Correction}\label{sec:data-augmentation}
In order to achieve a good network performance, the SSD and WCD traces as well as the arrival times and \xmaxmc{}, are augmented before being used for training.
For example when studying the pure-proton and pure-iron lines of the three hadronic interaction models shown in \Cref{fig:auger_xmax}, it is evident that \xmax{} values are correlated with energy.
This effect is known as the \emph{elongation rate}.
This connection between \xmax{} and energy is something the neural network would learn.
This could lead to the trained model predicting \xmax{} values solely based on the reconstructed energy of an event.
Such a behavior is not desired, as it would direct the network away from forming predictions based on differences in the SSD and WCD traces.
This energy dependence can be removed by subtracting the pure-iron elongation rate as implemented in EPOS-LHC \cite{EPOS-LHC} from every \xmaxmc{} value as follows
\begin{align}\label{eq:elongation-rate}
	X'_\mathrm{max,MC} = X_\mathrm{max,MC} - \left(\underbrace{\SI{600}{\gcm}}_{\substack{\text{additional} \\ \text{scaling} \\ \text{factor}}} + \underbrace{\SI{63.1}{\gcm}\cdot (E_\text{SD,rec} - 18) + \SI{1.97}{\gcm} \cdot (E_\text{SD,rec} - 18)^{2}}_{\substack{\text{EPOS-LHC elongation rate line}\\ \text{for iron}}}\right)\,.
\end{align}
In addition to the EPOS-LHC line, \SI{600}{\gcm} are subtracted in order to scale \xmaxmc{} down to smaller values, while keeping all values seen in simulation positive.
The 18 in \cref{eq:elongation-rate} stems from the $\log_{10}$ of the EPOS-LHC reference energy of \SI{1}{\exa\electronvolt} (\SI{e18}{\electronvolt}).
This provides an additional benefit, as this adjustment shifts the \xmaxmc{} values to a smaller scale.
Hereafter, \xmaxmc{} is simply used to refer to the adapted values $X'_\mathrm{max,MC}$.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.85\textwidth]{figures/06_Data_Preparation/Traces_oxygen_20.0_20.2.root_1017760102_5309_bef.png}
	\includegraphics[width=0.85\textwidth]{figures/06_Data_Preparation/Traces_oxygen_20.0_20.2.root_1017760102_5309_aft.png}
	\caption[SSD and WCD trace pre-processing]{Top row: WCD (left) and SSD (right) traces before pre-processing, showing the full trace of 2048 time bins. Bottom row: WCD (left) and SSD (right) traces after removing the leading zeros up 48 bins before the trigger and the empty bins at the and of the trace, keeping only the relevant signal.}
	\label{fig:trace_augmentation}
\end{figure}
\noindent

The signal traces $S(t)_\mathrm{SSD}$ and $S(t)_\mathrm{WCD}$ have a full length of 2048 bins, with a bin width of \SI{8.33}{\nano\second} each.
The length of the actual signal contained in these traces is generally much shorter and therefore the signal traces contain a lot of empty bins, as can be seen in \Cref{fig:trace_augmentation}.
This is especially true for a fixed number of bins at the start of each trace, which originates from the way the traces are read out when a trigger is received.
In the {\Offline{}} trigger simulation, this is characterized by the so-called \emph{latch bin}, which indicates the number of bins before the actual signal starts.
For UB stations it has a value of 246.
For upgraded stations with a UUB this is internally multiplied by a factor of three, to account for the additional bins.
The resulting 738 empty bins are unnecessary information for this analysis and are therefore removed, with a small buffer of $16\cdot3$\footnote{This was set as an offset of 16 bins from the UB latch bin value and is then adapted to the UUB} bins.
This results in 690 bins being cut from the start of the trace.
The next 400 bins of trace data are kept for both detectors and the remaining 958 bins at the end of the trace are also cut off.
On average the full trace information should be contained, as 400 bins represents a generous thickness of shower front of roughly $400 \cdot \SI{8.33}{\nano\second} \cdot c \approx \SI{1}{\kilo\meter}$ passing through the detector in that time.
Additionally, for the WCD case the mean over all three PMT traces is calculated, to have on input to compare the SSD trace to.

The arrival time $t_{i}$, in nanoseconds, for each station participating in an event is also transformed to the relative arrival time 
\begin{align}
	\Delta t_{i} = \frac{t_{i} - t_\mathrm{min}}{\SI{8.33}{\nano\second}}\,,
\end{align}
where $t_\mathrm{min}$, also in nanoseconds, is the earliest of all the arrival times in an event.
The division by the trace bin width of \SI{8.33}{\nano\second} is performed to adjust the arrival times from an absolute value of large magnitude $\mathcal{O}\left(\SI{e9}{\second}\right)$ to a small relative arrival time in units of trace bins.

\subsection{Axial Layout and Padding}
The network developed in this thesis is supposed to learn from a combination of the SSD and WCD trace information, as well as the stations relative position to each other.
Since the number of stations varies with each event, and the input shape for the neural network has to be the same for all events, it is important to unify the station layout in pre-processing.
In \Cref{fig:footprint_padding} an example of a shower footprint on the ground before pre-processing can be seen.

\begin{figure}[!htb]
	\centering
	\includegraphics[height=0.25\textheight]{figures/06_Data_Preparation/PP_oxygen_20.0_20.2.root_1569670101_bef.png}
	\includegraphics[height=0.25\textheight]{figures/06_Data_Preparation/PP_rotatedoxygen_20.0_20.2.root_1569670101_aft.png}
	\caption[Shower footprint before and after pre-processing]{Top: Original shower footprint of candidate stations. The size of each station represents the size of the total signal in the SSD, while the color represents the relative arrival time, calculated as mentioned above. Bottom: The same footprint after pre-processing with the gray dots representing the zero padding stations to fill in the axial mapping needed for the neural network. All stations are now centered around the hottest station, which is located at (\SI{0}{\kilo\meter},\SI{0}{\kilo\meter}).}
	\label{fig:footprint_padding}
\end{figure}
\noindent
For this thesis, the neural network was designed to process the event data in a $9\times9$ grid.
The first step of bringing the input data into this shape, is to create an array, $A_\mathrm{ideal}$, of station positions, $\vec{x}_{\mathrm{ideal},j}$, which are centered around the point (\SI{0}{\kilo\meter}, \SI{0}{\kilo\meter}) with a station spacing of \SI{1500}{\meter} and an axial layout.
Then, arrays of shape $9\times9\times d$ are created and initialized with zeros, where $d$ is the dimension of the respective future inputs, a list of which can be seen in \Cref{tab:input_dimensions}.
\begin{table}[!htb]
	\centering
	\caption[DNN input arrays, dimensions, and units]{Input arrays for the DNN to train on and their respective dimensions and units. \xmax{} is separated as it serves as the label that needs to be learned by the network.}
	\label{tab:input_dimensions}
	\begin{tabular}{l|l|l}  
		Input & Dimensions & Unit\\ \hline \hline
		SSD traces & array(9, 9, 400) & MIP\\ 
		WCD traces & array(9, 9, 400) & VEM\\ 
		Energy, zenith, time & array(9, 9, 3) & \lgE,rad,trace-bins\\ \hline
		\xmaxmc{} & array(1) & \si{\gcm}
	\end{tabular}
\end{table}

The input arrays are filled with the necessary data by looping over the station vector, which contains all stations that had a any signal at the time of the event.
Station information is only read out under the condition, that the current station in the loop is marked as a \emph{candidate station}.
A candidate station is a station that was selected by the trigger, simulated or real, as part of the event.
Only candidate stations are used as this can largely filter out random noise stations, which are also included in the station vector.
If the current station is flagged as a candidate, its trace information and the arrival time are read out and adapted as described above. 
The station position $\vec{x}_{i}$ is also saved.

In each event the station with the highest signal in the WCD is flagged as the \emph{hottest station}.
Its coordinates $\vec{x}_\mathrm{H}$ are then used as the reference point for all other stations contained in the event and their relative position to the hottest station is calculated as
\begin{align}
	\vec{x}'_{i} = \vec{x}_{i} - \vec{x}_\mathrm{H}\,.
\end{align}
The hottest station is also included which places it at the center of the array (\SI{0}{\kilo\meter}, \SI{0}{\kilo\meter}).
These new coordinates are then used to calculate the placement of the stations in $A_\mathrm{ideal}$.
This is done by calculating the euclidean distance $d_\text{euclid}$ of $\vec{x}'_\mathrm{i}$ to each ideal station position $\vec{x}_{\mathrm{ideal},j}$ in $A_\mathrm{ideal}$.
The ideal station with the minimal distance to $\vec{x}'_{i}$ represents the ideal position of that station $i$:
\begin{align}
	\min\left(d_\text{euclid}(\vec{x}'_{i},\vec{x}_{\mathrm{ideal},j})\right) = \min\left(\sqrt{\sum_{k}\vec{x}'_{i,k} - x_{\mathrm{ideal},j,k}}\right)
\end{align}
The index of this ideal station in $A_\mathrm{ideal}$ is then used to write the traces and arrival times to the correct place in their respective input arrays, replacing the zeros.
The relative arrival times $t_{i}$ are combined with \ESD{} and \thetaSD{} into one array.
Saving the energy \ESD{} and zenith angle \thetaSD{} values for each station seems excessive, considering they are event-level variables.
However, this is done to later ease their combination with the station-specific data.

The results of this procedure are zero padded arrays that contain the necessary input information at the relative station positions.
The footprint of the padded event data can be seen in \Cref{fig:footprint_padding} at the bottom.
The Python package NumPy \cite{numpy} is used for the majority of these operations and the input arrays are saved as compressed NumPy array files \texttt{.npz} for later used with the DNN described in \cref{chap:THEANALYSIS}.

\subsection{Removal of Geometric Degeneracy}
In addition to the data augmentations described earlier, a method was developed for this thesis with the goal of improving the performance and speed of the training of the neural network.
Since the number of stations participating in an event varies, a rather large grid would be needed to contain the entire footprint.
On top of that, large amounts of data are filled with zeros, from the zero-padding, which need to be processed by the network, especially for events with lower energies or zeniths and therefore smaller footprints.
Processing the input data as is, means that the positions of these ``inactive stations'' changes a lot in almost every event.
As a result of this the network needs a lot of nodes and a long time to converge.

\begin{figure}[!htb]
	\centering
	\raisebox{0.35\height}{\includegraphics[height=0.35\textwidth]{figures/06_Data_Preparation/rotation_withevent_andhighlight_nw.pdf}}
	\includegraphics[height=0.65\textwidth]{figures/06_Data_Preparation/combined_nw.pdf}
	\caption[Rotation of the grid mapping]{Left: All events are rotated to align with the $\phi_\text{SD,rec}=\SI{150}{\degree}$ axis, indicated by the blue wedge. Top right: An example event is shown with the initial orientation of the station mapping grid indicated by a solid gray line. The dashed gray line indicates a rotated version of the mapping that contains all stations in the event. Bottom right: The final event that is used for training.}
	\label{fig:mapping_rotation}
\end{figure}
\noindent

To combat this, a rotation of the mapping was developed, which results in the positions of ``active station'' varying less from event to event.
The network then ``sees'' the inactive stations more commonly at the same position in the input array and can adapt the activation of nodes accordingly.
This technique also allows for the total number of nodes in the network to be reduced.
This is carried out by rotating the mapping of for each individual event so that the azimuth angle of each event always aligns with the same axis of the mapping grid.
As can be seen in \Cref{fig:mapping_rotation}, the north-west/south-east azimuth axis of the $9\times9$ grid covers only a distance of $\approx\SI{12}{\kilo\meter}$, while the south-west/north-east axis covers a much larger distance of $\approx\SI{21}{\kilo\meter}$.
Thus, the long axis of the axial grid is chosen as the axis to which each event is rotated to align with.
This is indicated in \Cref{fig:mapping_rotation} on the left by the blue wedge.
With this last step the AugerPrime simulations are ready to be used as an input for the DNN.

