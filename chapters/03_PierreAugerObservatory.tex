\chapter[THE PIERRE AUGER OBSERVATORY]{The Pierre Auger Observatory}\label{chap:PAO}
\epigraph{\itshape\myopeningquote Instruments only register things they are designed to register. Space still contains infinite unknowns\myclosingquote}{--- Mr. Spock, \textit{Star Trek: The Original Series}}

The Pierre Auger Observatory is the world's largest detector for measuring the extensive air showers produced by cosmic rays. It is located in the Pampa Amarilla near the city of Malargüe in the province of Mendoza, Argentina \cite{NIMpaper}.
It was completed in 2008 and covers a surface area of around \SI{3000}{\square\kilo\meter}.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.6\textwidth]{figures/03_PierreAugerObservatory/PAO_Layout.png}
	\caption[Layout of the Pierre Auger Observatory]{Layout of the Pierre Auger Observatory in Argentina. Each black dot represents a WCD, while the lines indicate the field of view of each telescope at the different FD sites.}
	\label{fig:PAO}
\end{figure}
\noindent

There are two main detectors used at the Observatory, the \gls{fd} and the \gls{sd}.
The SD and FD will be covered in this thesis in further detail.
Information on other detector components such as the \gls{rd} or the \gls{umd} can be found in \cite{AERA,AERA2,APRadio} and \cite{UMD,APDesign}, respectively.
These different detector types are operated as a hybrid system to seize the advantages of each detector type, making the Pierre Auger Observatory a hybrid Observatory.
The largest part of the Observatory in terms of exposure is the SD which consists of 1660 \gls{wcd}.
The atmosphere above the SD is monitored by the 27 telescopes of FD, with six telescopes at each of the four FD building sites and an additional three in the low energy extension \gls{heat} \cite{HEAT}.
All these components can be seen in \Cref{fig:PAO}.
With these two main detectors, a complimentary measurement of the shower development in the atmosphere, via the FD, as well as its signal on the ground, via the SD, is possible, forming the heart of the hybrid detector \cite{NIMpaper}.
The RD, part of the ongoing Observatory upgrade, AugerPrime, is based on the Auger Engineering Radio Array (AERA) and will add another high statistics detection method to events with zenith angles above \SI{60}{\degree}.

\section{The Fluorescence Detector}\label{sec:FDAuger}
The charged particles created in an EAS will excite nitrogen molecules in the Earth's atmosphere as they pass.
These will then emit fluorescence light in the UV range, \ie in the wavelength range \SIrange{300}{430}{\nano\meter}.
The number of fluorescence photons emitted is directly proportional to the electromagnetic energy lost by the charged particles of the EAS and therefore also the total number of particles in the EAS.
Because of this, by detecting the fluorescence emission throughout the EAS the FD directly measures its energy deposit as a function of its atmospheric depth, $\frac{dE}{dX}$, also known as the longitudinal development profile of the shower.
The integral of the profile
\begin{align}
	E_\text{cal} = \int\frac{dE}{dX}dX
\end{align}
is directly proportional to the electromagnetic energy of the EAS, which comprises $\sim\SI{90}{\percent}$ of the primary particle's energy, thus providing a nearly calorimetric measurement \cite{FDarticle}.
The depth of shower maximum, \xmax{}, is also easily inferred from the FD measurement and because it is roughly proportional to the logarithm of the primary mass, $\ln(A)$, the FD is sensitive to the composition of the primary.
Because the FD directly measures \xmax{}, it currently has the best resolution for shower-by-shower composition measurements among the detectors at the Observatory and in general.

The instrumentation of the FD primarily consists of 24 telescopes overlooking the array housed in four different FD sites or \emph{Eyes}: Coihueco, Los Morados, Loma Amarilla and Los Leones, indicated in \Cref{fig:PAO}.
At one of these (Coihueco) the low energy extension HEAT observess the upper atmosphere directly above a 66-detector sub-array of the SD. In this \SI{23.5}{\square\kilo\meter} region the reduced \SI{750}{\meter} spacing of the detector stations and the higher observation level of HEAT allow for measurements of events down to nearly PeV energies.
Each fluorescence telescope essentially consists of a UV filter window, a \SI{13}{\meter\squared} segmented spherical mirror and a camera which consists of 440 \glspl{pmt}.
The telescope is designed to give the camera a fixed \SIrange[range-phrase=$\times$]{30}{30}{\degree} field of view (FoV), which results in each PMT observing a particular $\sim\SI{1.5}{\degree}$ section of the sky.

\begin{figure}[!htb]
	\centering
	\includegraphics[height=0.35\textwidth]{figures/03_PierreAugerObservatory/FD_outside.jpg}
	\includegraphics[height=0.35\textwidth]{figures/03_PierreAugerObservatory/FD_inside.png}
	\caption[FD buildings and telescope]{Left: FD building from the outside, with three open telescopes visible. Right: Inside view of an FD telescope with the main components labeled. From \cite{NIMpaper}.}
	\label{fig:FD}
\end{figure}
\noindent

During an FD measurement, first, most background light is removed from the UV fluorescence light of the shower via the filter window.
The light is then gathered by the mirror and reflected onto the PMT camera, which sits in the focal plane of the mirror.
Each PMT then records the UV light within its FoV with a \SI{100}{\nano\second} timing resolution in an intensity and timing trace of the shower.
From this the shower geometry and profile can be reconstructed \cite{FDarticle}.

The FD requires very dark, cloudless nights to make a measurement.
Because of this, it has a low duty cycle of \SIrange[range-units=single, range-phrase=--]{14}{15}{\percent}, resulting in relatively poor event statistics at the highest energies.
As the duty cycle of the SD (see \cref{sec:SDAuger}) is much higher than the FDs, the SD collects much higher event statistics at the highest energies.
The composition resolution of SD-only analyses however is not as good as that of the FD.
Because of this, an update to the SD to enhance its shower-by-shower composition measurements capabilities, such as the currently on-going upgrade AugerPrime, has the potential to greatly increase composition information at the highest energies.
Development of a technique to achieve this is the motivation of this thesis.

\section{The Surface Detector}\label{sec:SDAuger}
As mentioned before, the standard SD primarily consists of 1600 WCDs.
These WCDs are placed in a hexagonal grid with a spacing of \SI{1500}{\meter} between stations.
Each WCD consists of a plastic tank with a diameter of \SI{3.6}{\meter}, which is filled with \SI{12000}{\litre} of highly purified water. 
Three PMTs, of \SI{9}{inch} diameter each, monitor this water to measure the Cherenkov light produced when the particles of an air shower pass through the water \cite{NIMpaper}.
Each of those PMTs is placed \SI{1.2}{\meter} from the center of the water tank, looking downwards in a symmetric layout.
The water itself is held inside an opaque, highly reflective liner composed of Tyvek$^{\copyright}$.
The high purity water is necessary to achieve the lowest possible attenuation for Cherenkov light, while also maintaining the stability of the water and liner properties over the long operation time of the Observatory.

\begin{figure}[!htb]
	\centering
	\includegraphics[height=0.3\textwidth]{figures/03_PierreAugerObservatory/SD.png}
	\includegraphics[height=0.3\textwidth]{figures/03_PierreAugerObservatory/WCD_schematic.jpg}
	\caption[Components of a WCD]{Left: Example of a WCD in the array with the different components marked. Right: An inside view illustration of such a WCD with the PMTs shown and a light producing particle going through. From \cite{NIMpaper}.}
	\label{fig:WCD}
\end{figure}
\noindent

Each of these SD stations is fully self-sufficient and autonomous, via its GPS receiver, radio communications antenna, micro-controller, and a solar panel with a battery, which supply the 10 Watts of power needed by the electronics and PMTs.
The full setup of such a station can be seen in \Cref{fig:WCD}. 
Because each SD station is relatively unaffected by the environment, the SD has a duty cycle of nearly \SI{100}{\percent} and constantly records the intensity and timing of particle flashes passing through it.

\subsection{The SD Signal Calibration}\label{sec:SDCalib}
The electronics of the SD consist of six \SI{40}{\mega\hertz} \glspl{fadc}, which digitize the signals of the three PMTs in both a \gls{lg} channel and a \gls{hg} channel which is amplified by a factor of 32 \cite{NIMpaper}.
These two channels together provide enough dynamic range to measure the flux of particles well, both near the shower core ($\sim\SI{1000}{\particle\per\micro\second}$) and far from it ($\sim\SI{1}{\particle\per\micro\second}$).
For both of these FADC channels, a signal trace is recorded with a length of 768 bins for each PMT.
Due to the above mentioned sampling rate of \SI{40}{\mega\hertz}, each bin has a time width of \SI{25}{\nano\second}.

The signal of each detector station differs and therefore has to be converted to units of a \gls{vem}.
One VEM is defined as the charge deposited in that station by one \gls{vct} muon.
A VEM serves as the unit of measurement for the signal created by particles passing through the stations, and is needed in order to have a universal measurement standard for all stations.
While the SD is not able to filter out and measure only VCTs, calibration is possible by using the response of the PMTs to random atmospheric muons in combination with reference measurements from a muon telescope which precisely measures VCTs.

To perform the SD calibration mainly two calibration histograms are used, the charge histogram (for each PMT and their sum) and the pulse height histogram (for each PMT and their sum).
The charge that corresponds to a VEM, is obtained by integrating the amplitudes in time over the signal pulse duration.
As can be seen in \Cref{fig:VEM}, the Cherenkov light created by atmospheric muons produces a peak in the charge histogram, which, with enough events, always corresponds to $\mathrm{Q^{peak}_{VEM}}\sim\SI{1.09}{\vem}$.
This peak is used together with the reference measurement of $\mathrm{Q_{VEM}}=\SI{1}{\vem}$ of the muon telescope (also shown in \Cref{fig:VEM}) to obtain the calibration value needed to scale $\mathrm{Q^{peak}_{VEM}}$ for the sum of all three PMTs and thereby express the charge in VEM  \cite{SDCalib}.
Each of these is created every minute, which means that in the case of an event trigger in the array, there is calibration data available from the previous minute, guaranteeing high calibration accuracy.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.8\textwidth]{figures/03_PierreAugerObservatory/VEM.png}
	\caption[Charge Histogram]{Left:The charge histogram (black), is the sum of the responses of all three PMTs to background noise from the last \SI{60}{\second}, while the dashed red line represents the reference measurement done by an external muon telescope. The first peak in the charge histogram is noise from low-energy particles while the second peak is used to calibrate the unit of VEM in the corresponding station. Right: The pulse height histogram (black) that is used to calibrate $\mathrm{I^{peak}_{VEM}}$ to be used for trigger thresholds. This is again done with the red dashed line from the muon telescope measurement as reference. Taken from \cite{SDCalib}.}
	\label{fig:VEM}
\end{figure}
\noindent

All triggered events of the SD require the signal trace, a measure of the current seen by the PMT which is expressed in ADC counts, to be over a certain threshold.
Therefore, the station current must also be calibrated to a reference unit $\mathrm{I_{VEM}}$.
To do this, the same technique as is used for the VEM is employed.
Atmospheric muons will also produce a peak current $\mathrm{I^{peak}_{VEM}}$ in the pulse height histogram.
$\mathrm{I^{peak}_{VEM}}$ is then also calibrated via the reference current from the muon telescope, $\mathrm{I_{VEM}}$ \cite{SDCalib}.

\subsection{The SD Trigger System}\label{sec:SDTrigger}
Overall, there are three main trigger functions implemented in the front-end electronics of each station.
These are the scaler trigger, calibration trigger and main shower trigger.
While the scaler trigger, which records pulses with a very low signal threshold, is useful for performing complementary physics such as space weather \cite{SpaceWeather}, the calibration trigger is crucial for operating the detector.
The calibration trigger is set so that it measures low pulses from each PMT for 20 bins as soon as any bin sees a current greater than \SI{0.1}{\Ivem}.
This data is then used to perform the calibration processes described earlier.

The most important trigger is the main shower trigger.
Because of the very high rate of random accidental muons and the very low rate of UHECRs, obtaining every single measurements from every station is decidedly impractical.
This makes it necessary to define a set of conditions that need to be fulfilled in order for an event to be classified as coming from an UHECR.
This trigger system is hierarchical and has five levels, T1 through T5.
The first three levels provide the foundation of all later trigger levels and are illustrated in \Cref{fig:SDTrigHirarchy}.
%The first two levels are station-internal and have special trigger modes each.

The first level, T1, consists of two triggers, a simple threshold (TH) trigger, which is activated if all three PMTs record a signal above \SI{1.75}{\Ivem}, and a time-over-threshold (ToT) trigger.
The ToT requires a signal over a threshold of \SI{0.2}{\Ivem} in at least two PMTs in a time window of 120 bins which lasts for a minimum of 13 bins.
The T1-TH is especially targeted at identifying the larger, inclined events, where the muonic component dominates.
The T1-ToT is targeted at smaller, more vertical events, low-energy showers close to the shower core, and high-energy showers far from it.
In 2013, two additional trigger methods, ToTd and MoPS, were added to reduce the influence of single muons and lower the energy threshold of the array and are described in more detail in \cite{NIMpaper}.
%Both require the integrated signal to be greater than \SI{0.5}{\vem}.
%The ToTd trigger introduces an additional step of de-convolving the exponential tail of the diffusely reflected Cherenkov light pulses
%before applying the previously described ToT.
%The \emph{multiplicity-of-positive-steps} (MoPS) trigger counts the number of positive bins in a \SI{3}{\micro\second} sliding window across the signal once the bins are above a threshold of $\sim 5\times$baseline.

\begin{figure}[!htb]
	\centering
	\includegraphics[height=0.13\textheight]{figures/03_PierreAugerObservatory/TriggerHirarchy.png}
	\includegraphics[height=0.13\textheight]{figures/03_PierreAugerObservatory/EventSelectionHirarchy.png}
	\caption[SD low- and high-level trigger]{Left: Schematic overview showing the low-level trigger hierarchy, that events need to pass to be send to CDAS. Right: Schematic overview showing the high-level trigger hierarchy, that is needed to differential air shower events from accidental trigger by \eg atmospheric muons. Taken from \cite{SDTrigger}.}
	\label{fig:SDTrigHirarchy}
\end{figure}
\noindent

For the second level, T2, the previously identified T1-ToT triggers are always promoted to T2-ToT, while the T1-TH triggers have to fulfill an additional criterion of a 3-fold PMT coincidence with each PMT being over a signal threshold of \SI{3.2}{\Ivem}.
All T2 triggers are sent with their timestamp via wireless communication to the \gls{cdas} (see \Cref{fig:SDTrigHirarchy}) to be combined with the signals from others detectors to form an event.
The first step towards forming an event is the third level, T3, in the trigger hierarchy.
It is again split into two different modes, both of which involve the fulfillment of many spatial and temporal conditions.
Examples of both modes can be seen in \Cref{fig:SD_Trigger}.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/03_PierreAugerObservatory/SD_trigger.png}
	\caption[SD T3 trigger example configurations]{The two different types of T3 trigger. Example configurations of triggered central stations and coincidentally triggering stations in the surrounding hexagons are circled. Taken from \cite{SDTrigger}.}
	\label{fig:SD_Trigger}
\end{figure}
\noindent

T3 is based on coincident signals of a station and its surrounding hexagons, with $C_\mathrm{n}$ used in the naming scheme defining the $n^\mathrm{th}$ set of neighbors, \ie the order of surrounding hexagons.
For the $\mathrm{ToT}2C_{1}\&3C_{2}$ trigger mode at least three participating detectors need to have passed the T2-ToT trigger.
An additional requirement of compactness (see \Cref{fig:SD_Trigger} left) is also imposed.
The $2C_{1}$ in the name references that at least one station needs to be within the $C_{1}$ hexagon and likewise $3C_{2}$ indicates for the third station to be at least as close as the $C_{2}$ hexagon.
This trigger also has a temporal requirement which demands that the T2 triggers of the other stations arrive within \SI[parse-numbers=false, number-math-rm= \ensuremath]{(6 + 5C_\mathrm{n})}{\micro\second} of the first one \cite{SDTrigger}.
The second T3 builds on this but is of a less restrictive nature.
All stations must have either ToT-T2 or TH-T2 triggers (see \Cref{fig:SDTrigHirarchy}) and a 4-fold coincidence is required.
The first two neighbors of a selected station must be in the same order as for the $\mathrm{ToT}2C_{1}\&3C_{2}$, whereas the fourth station can be as far away as the $4^\mathrm{th}$ order.
This trigger is accordingly named $2C_{1}\&3C_{2}\&4C_{4}$ and is especially targeted at inclined showers, with a larger shower footprint on the ground.

After these steps, further event selection is done according to the trigger levels T4 and T5, as displayed in \Cref{fig:SDTrigHirarchy}.
Not all the triggers that pass the T3 level are real air shower events, so a physics trigger, T4, is needed to select only those events likely caused by EAS.
There are again two different sets of T4 triggers.
The first one, 3ToT, requires the arrival times in at least three additional stations in a triangle, that have passed the T2-ToT, to fit to a planar wave front traveling at the speed of light.
This is particularly effective at picking out events with a zenith angle of less than \SI{60}{\degree}.
The second type of T4 trigger, 4C1, adds to that by selecting events with four or more stations, that have passed any kind of T2 level trigger and can also be fit with a planar wave traveling at the speed of light.

Finally, the fiducial trigger, T5, removes events which are close to the borders of the array and thus may have missing stations in the footprint, which could be leading to wrong reconstructions.
To achieve this, T5 only selects an event if the station with the highest signal has all its 6 closest surrounding neighbors, \ie $C_{1}$, working at the time of measurement.
This selections ensures that high energy events that fall close to the border, but inside the array, may still be kept and reconstructed while limiting potential biases from incomplete events \cite{SDTrigger}.

\subsection{The SD Shower Reconstruction}
An in-depth overview on the SD shower reconstruction is given in \cite{SDReconstruction}.
The most important step for the analyses presented in this thesis are the reconstruction of the shower geometry and the shower energy. 
First, the arrival direction of the shower is approximated assuming a spherical shower front
\begin{align}
	c(t_{\mathrm{sh},i}-t_{0})=\left|\vec{x}_{0}-\vec{x}_{i}\right|\,,
\end{align}
with the speed of light $c$.
Here, $t_{\mathrm{sh},i}$ is the arrival time of the shower front at a station $i$ with position $\vec{x}_{i}$.
The parameters $t_{0}$ and $\vec{x}_{0}$ are the start time and position of the shower.
The arrival direction obtained this way can then be used to calculate the shower core position $\vec{x}_\mathrm{c}$ as well as the shower size by using the log likelihood method and simultaneously fitting the LDF of the form
\begin{align}
	S(r) = S\left(r_\mathrm{opt}\right)\left(\frac{r}{r_\mathrm{opt}}\right)^{\beta}\left(\frac{r+r_{s}}{r_\mathrm{opt}+r_{s}}\right)^{\beta+\gamma}
\end{align}
where $S$ is the signal at the perpendicular distance to the shower axis $r$.
The parameter $r_{s}$ is fixed to $r_{s}$=\SI{700}{\meter} and $r_\mathrm{opt}$ denotes the optimum distance based on the array spacing.
For the SD array with the spacing of \SI{1500}{\meter}, this amounts to $r_\mathrm{opt}\approx$\SI{1000}{\meter}.
Once the position of the shower core $\vec{x_\mathrm{c}}$ is found, the shower axis $\vec{a}$ can be determined as
\begin{align}
	\vec{a} = \frac{\vec{x}_\mathrm{0}-\vec{x}_\mathrm{c}}{\left|\vec{x}_\mathrm{0}-\vec{x}_\mathrm{c}\right|}\,.
\end{align}

The parameter $S(1000)$ is an estimator of the shower size and thus related to the primary energy of the EAS.
To reconstruct the primary energy from $S(1000)$, atmospheric attenuation has to be considered.
With increasing zenith angle the slant depth to ground of the SD increases.
As a result, very inclined showers are overall detected at a later shower ``age'' and are subject to stronger particle attenuation and geometric effects.
This leads to the shower size $S(1000)$ having zenith dependence.
To correct for this effect a constant intensity cut (CIC) method is used described in detail in \cite{GaisserHillas, ForS38}.
This results in an attenuation function that can be used to convert $S(1000)$ to a zenith independent shower size.
Assuming that the arrival rate of cosmic rays above a given energy does not depend on zenith angle, a minimum $S(1000)_\mathrm{min}$ should exist above which the arrival rate of a larger shower size should be equal to a constant at a given zenith angle $\theta$.
Once this attenuation function $f_\mathrm{CIC}(\theta)$ is found, it can be used to convert $S(1000)$ via
\begin{align}
	S_\mathrm{38}=\frac{S(1000)}{f_\mathrm{CIC}(\theta)}\,,
\end{align}
where 38 refers to the median zenith angle of all events of $\overline{\theta}=\SI{38}{\degree}$.
This transforms $S(1000)$ into the equivalent size the shower would have had, had the primary particle arrived with a zenith angle of \SI{38}{\degree}.
Using hybrid events and the nearly-calorimetric energy measurement of the FD, this parameter can then be calibrated \cite{SDECalib} by fitting with a power-law calibration curve 
\begin{align}
	E_\mathrm{FD}=A\left(\frac{S_\mathrm{38}}{\mathrm{VEM}}\right)^B \,.
\end{align}
Using $A$ and $B$ from this fit an energy can be assigned to every SD event
\begin{align}
	E_\mathrm{SD}=A\left(\frac{S(1000)}{f_\mathrm{CIC}(\theta)\times\mathrm{VEM}}\right)^B \,,
\end{align}
also referred to in this document as $E_\mathrm{SD,rec}$.

\section{The AugerPrime Upgrade}\label{sec:AugerPrime}
The AugerPrime upgrade of the Pierre Auger Observatory was conceived in 2015 \cite{APDesign}, with the construction of the first prototypes starting in 2016.
It is primarily an improvement and extension of the detector system and consists of five main aspects:
\begin{enumerate}
	\item The installation of plastic scintillators on top of the existing WCDs, which provide a complementary measurement of air showers.
	A \gls{ssd} is more sensitive to the electromagnetic component of an EAS, while the WCD has a stronger response to the muonic component.
	These differing responses between the two detectors will enhance the precision of primary particle composition
	measurements made by the SD on an event-by-event basis.
	The SSDs are also easily deployable, which is important considering the full array size \cite{SSDarticle}.
	\item The replacement of the SD electronics, specifically the \gls{ub} and the separate SD front-end board, with an upgraded version, the \gls{uub} that combines the two.
	The UUB is a significant improvement because it is capable of processing both the WCD and SSD signals and has a faster sampling rate, better timing accuracy, and higher dynamic range.
	Additionally, a fourth \emph{small PMT} (sPMT), is being installed inside the WCD to extend its dynamic range \cite{UUBarticle,DYNarticle}.
	\item The increase of the FDs duty cycle by $\sim\SI{50}{\percent}$ by extending its observation time to periods with higher night sky background.
	\item The deployment of an \emph{underground muon detector} (UMD) in the \SI{23.5}{\kilo\meter\squared} sub-array of the SD (\SI{750}{\meter}), to provide direct muon measurements. This can be seen as an extension of the currently used AMIGA underground muon detectors.
	\item The deployment of radio antennas on top of each SD station \cite{APRadio}.
	This enables complementary radio measurements of air showers.
	It serves a similar purpose as the SSD, but in the very inclined zenith range (\SIrange{65}{85}{\degree}), whereas the SSD is sensitive up to $\sim\SI{60}{\degree}$.
\end{enumerate} 
In the context of this thesis, the most important parts of this upgrade are the addition of the SSD and the electronics upgrade.

%\subsection{Physics Goals}
After the extended operation of the Pierre Auger Observatory until the 2030s, the event statistics will have more than doubled.
Furthermore, with the upgrade all events taken will have additional mass sensitivity compared to the existing data set.
As mentioned earlier, this additional information can offer an improved understanding of the composition of primary particles at the highest energies \cite{APDesign}.
This could also lead to a reduction in systematic uncertainties related to modeling hadronic showers via Monte Carlo simulations, by providing a better understanding of the particle contents of showers at energies well above what is currently available at colliders.
The knowledge gained from this could be used to reanalyze old data, especially in the context of improving energy reconstructions in regards to the current muon deficit, where the muon content in EAS simulations is much lower than the one detected in data \cite{MuonDeficit}.
Lastly, it should help in photon and neutrino searches, through increased sensitivity.

\subsection{The Surface Scintillator Detector}
The SSD was chosen with the goal of providing a different detector response than the WCD, while keeping the need for robustness and minimal maintenance in mind.
A schematic of the resulting design of the SSD is shown in \Cref{fig:SSD}, as well as an example of one of the upgraded stations (without the RD) from the AugerPrime \gls{ea}.

Each SSD consists of two scintillator parts with a combined area of $\sim\SI{3.8}{\meter\squared}$ \cite{SSDarticle}.
The parts consist of 12 bars made from extruded polystyrene with emission lines between \SIrange{330}{480}{\nano\meter}.
%Each of the bars measures \SI{1}{\centi\meter}$\times$\SI{10}{\centi\meter}$\times$\SI{1.6}{\meter} and is coated with TiO2 for reflectivity to keep cross-talk between the individual bars minimal and also protect them from outside influences.
The light in the scintillator bars is collected by wavelength-shifting fibers.
Their absorption spectrum matches the emission of the scintillator material, while their own emission spectrum is above \SI{450}{\nano\meter}.
Special focus was put on the routing of these fibers and guiding them to the front of the PMT to ensure low light loss.
For this, each fiber starts and ends at the \emph{cookie}, which bundles all fiber ends in front of the PMT.
The fibers themselves are passed through the scintillator bars by guiding them in U-turns from one bar to another before returning to the cookie.
From the cookie to the start of a scintillator bar the light needs to travel $\approx\SI{1.1}{\meter}$, which results in only photons with a wavelength above \SI{500}{\nano\meter} surviving due to wavelength shifting and attenuation \cite{SSDarticle}.


\begin{figure}[!htb]
	\centering
	\includegraphics[height=0.26\textwidth]{figures/03_PierreAugerObservatory/ssd_schematic.png}
	\includegraphics[height=0.26\textwidth]{figures/03_PierreAugerObservatory/twins.png}
	\caption[SSD schematics and example of SSD in the array]{Schematic view of the scintillator plane and its components (left) and assembled upgraded station in the Engineering Array (right).}
	\label{fig:SSD}
\end{figure}
\noindent

The overall shift to larger wavelengths is important, because the quantum efficiency of the PMTs selected for the SSD is higher for these larger wavelengths than for those of the light emitted directly from the scintillator.
The PMT chosen for the upgrade is the model R9420 manufactured by Hamamatsu, which has a \SI{1.5}{inch} bi-alkali photo-cathode specifically suited for scintillator measurements \cite{SSDarticle}.
Its high voltage power supply is soldered to the PMT based on a custom-made design by the ISEG company.
The calibration of this PMT is carried out much in the same way as those in the WCDs.
The only major difference is the calibration unit involved, as the scintillator is more sensitive to electromagnetic particles rather than muons.
Hence, the calibration unit of the SSD is the number of \gls{mip}.
Further details and example charge histograms are provided in \cref{sec:MIP}.
The triggering of an upgraded station does not differ much from the WCD-only trigger, as the SSD is only operated in a passive mode, meaning that the SSD data is read out, if the WCD triggers.
Adjustments to the existing triggers are only necessary in regards to the change in the sampling rate.

\subsection{The Upgraded Unified Board}\label{sec:UUB}
The UUB was designed to improve on a few aspects with respect to the original UB in combination with the SD front-end board.
These improvements include better data quality, timing accuracy, and an increased dynamic range \cite{UUBarticle}.
The data quality is improved by using a higher sampling rate of \SI{120}{\mega\hertz} as compared to the \SI{40}{\mega\hertz} of the UB, resulting in signal traces with shorter time bins of \SI{8.33}{\nano\second}.
It is important that backwards compatibility with the current WCD-only data-sets is maintained and so the total time-span of the SD traces is kept nearly the same.
The increased sampling rate means that while the UB reads out the WCDs signals with 768 time bins of \SI{25}{\nano\second}, the UUB reads signals out with 2048 bins.
This means that, while the current triggers must be adapted to the higher sampling rate, it is also possible to use them as is on the upgraded board by down-sampling \cite{UUBarticle}.
All the data in \cref{chap:EAData} has been obtained with this down-sampling method.
The improvement in detector timing is achieved by the higher sampling rate, which gives a more precise measurement on the start time of the signal trace, as well as a new GPS module.
For more information on the timing see \cref{sec:timing}.

The extended dynamic range, matched between SSD and WCD, will specifically allow for measuring signals at distances as close as \SI{250}{\meter} from the shower core without saturating \cite{DYNarticle}.
The dynamic range in the SSD is similar to that described in \cref{sec:SDTrigger}, as it has a HG and an LG channel.
In the SSD, the HG signal is amplified by a factor of 32 while the LG is attenuated by a factor of 4.
The effect of these gain settings is further studied in \cref{sec:charge}.
To achieve a comparable dynamic range in the WCD a hardware extension is required which is, as mentioned above, achieved by the addition of the sPMT \cite{DYNarticle}.

\subsection{Deployment of AugerPrime}
The deployment of SSDs began with the assembly of the AugerPrime EA, which has been taking data in the array since October 2016

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/03_PierreAugerObservatory/deployment.pdf}
	\caption[AugerPrime deployment]{Overview of the deployment (status 07.04.2021). Non-upgraded stations are shown as circles (gray), stations with SSDs but without PMT as down-triangles (blue), stations with both an SSD and PMT as up-triangles (orange), and PPA stations are indicated by a plus (green).}
	\label{fig:SSD-PPA-Deploy}
\end{figure}
\noindent

In the EA, the upgraded detector design and firmware are frequently undergoing changes in order to improve their performance and reliability.
Many of these checks and changes are documented in \cref{chap:EAData}.
The EA consists of twelve stations.
The details of its configuration can be found in \Cref{fig:SSD-EA}.

Following the analysis works presented in \cref{chap:EAData}, as well as \cite{EAanalysis}, a so-called \gls{ppa} of another 76 SSDs was deployed in 2018 \cite{PPAanalysis}.
Due to limitations in the production of the UUBs, these SSDs are all connected to the existing UBs in place of one of the three WCD PMTs.
This provides the opportunity to already study and compare SSD and WCD data with higher statistics than is possible with the EA.% due to the \SI{1500}{\meter} spacing.
While the production of UUBs is still on-going, and the PMTs are still being tested, the deployment of the remaining SSDs is already in progress and can also be seen in \Cref{fig:SSD-PPA-Deploy}.

\section{The CDAS Software}
The CDAS of the Pierre Auger Observatory mentioned in \cref{sec:SDCalib} is a set of hardware and applications built to gather and process the data from the Observatory.
This includes building SD events and, with the FD data acquisition system, identifying and combining the data for hybrid events.

Since the beginning of AugerPrime, the CDAS software has undergone many changes and revisions to accommodate the electronics upgrade of the SD, and the additional signals from the SSD and the sPMT.
These changes mainly include accounting for the additional PMTs and their calibration, as well as processing the higher sampling rate mentioned in \cref{sec:UUB} and the update the SD triggers need to take advantage of it.
In addition to the PMT traces, additional monitoring information such as the HV of the PMTs, the MIP histograms, and the UUB temperatures, are directly extracted using CDAS.
This additional monitoring data is critical to the analysis of the EA performance described in \cref{chap:EAData}.
For further information and more details on the CDAS software see \cite{CDAS}.

\section{The \Offline{} Framework}
Most modern astrophysics experiments, especially ones the size of the Pierre Auger Observatory, must deal with thousands of detector responses from different detector types, which each use different variables to describe the quantities measured.
To take care of this, and standardize analyses for all detector measurement types for everyone in the Pierre Auger Collaboration, the \Offline{} Framework was conceived \cite{OFarticle}.
As such, the \xmax{} studies presented in this thesis are based on SD simulations that are carried out within the \mbox{\Offline{}} framework.
This framework is written in C++ and builds on three core components, which are visualized in \Cref{fig:offline}.
\begin{description}
	\item[Detector description:] It is a structure that summarizes all available data concerning the different detectors.
	This includes their layout and time dependent functionality, as well as conditions and environment at the Observatory at the time of a measured event.
	This is used both for real events, and simulated events, providing the ability to describe different detector configurations, which allows for a wide range of analysis applications.
	\item[Event data:] The event data structure is build to contain every variable which is used by multiple modules or in high-level analyses.
	This includes a wide range of quantities from detector responses and atmospheric conditions, to shower parameters such as energy or \xmax{}.
	This separation of data storage and data processing enables the unique interchangeability of modules within an analysis sequence.
	For real data, it starts out with just the detector responses, while the reconstructed event information is subsequently filled in by the various modules.
	For simulated events, the first module reads in a simulated particle shower and is followed by an event generator which determines the highest level characteristics of the event, such as time and geometry.
	Detector responses are then simulated and placed in the data structure.
	After this the event reconstruction procedure is the same as for real data.
	In the end the event data structure of both real and simulated events is exactly the same except the simulated event contains additional information concerning the simulation parameters used to create the event data.
	\item[Algorithms:] Every application performed with \Offline{}, from analysis to detector simulation, is built on analysis modules.
	These are sub-programs which carry out specific tasks, interacting only with the detector description and event data structure.
	This ensures the compatibility of all models with one another.
	For example, to carry out an analysis one only has to string together the necessary models inside a so-called \emph{module sequence}.
	Parameters that are adjustable can then be set in an XML file, called the \emph{bootstrap}.
	Pre-defined sets of module sequences and bootstraps are available to carry out standard detector simulations, reconstructions, and analyses, that can mostly be used without needing alterations by the user.
\end{description}

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.58\textwidth]{figures/03_PierreAugerObservatory/offline_framework_make_your_own.png}
	\caption[\Offline{} framework schematic]{Schematic overview of the general structure of the \Offline{} framework. Taken from \cite{OFarticle}.}
	\label{fig:offline}
\end{figure}
\noindent

\subsection{Advanced Data Summary Trees}
\glspl{adst} are a data structure developed within the Pierre Auger Collaboration to contain all high level variables needed for analyses, and optionally low level data \cite{GAP-2006-081}.
All the necessary information obtained from the above mentioned software components can be stored as an ADST in the ROOT format and be used to browse events and carry out physics analyses.
The software to create ADST files can be installed with the \Offline{} Framework or as a standalone.

\subsection{The Python tool PyIK}\label{sec:pyik}
The \gls{pyik} was developed at the Karlsruher Institut für Technologie.
In early versions, it contained a module that was developed to read out variables from ADST ROOT files and to process them directly in Python or save them to files, which are more easily read by Python for later analysis.
The module has since evolved into a bigger tool \cite{pyik_new} aimed at simplifying common analysis tasks in particle physics, with the ADST reading tool removed to make it available for a broader audience.
Hence, an older version of PyIK is used in this thesis to read in the SD simulations for further processing.
This version of PyIK is currently not available for download anymore.
% required version of PyIK can be downloaded from \cite{pyik}\todo{this link does not work anymore, update text and fix}
