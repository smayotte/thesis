import matplotlib.pyplot as plt
import matplotlib.image as mpimg

# def function(x):
# y = []
# z = []

# #Create two lists of matching lat and long coordinates
# for i in x:
#     y.append(i[1])
#     z.append(i[2])

#Turn off axis and axis ticks
# plt.tick_params(labelbottom='off', labelleft='off', bottom='off', left='off')
# plt.title("Bird Sightings")

#import map Image
img = mpimg.imread('map.png')

MAP_EXTENT = (-68.1866, -69.8154, -34.9107, -35.6930)
plt.imshow(img, zorder=0, extent=MAP_EXTENT)

plt.scatter(z, y, zorder=1)

#aspect=img.shape[0]/float(img.shape[1])*((MAP_EXTENT[1]-MAP_EXTENT[0])/(MAP_EXTENT[3]-MAP_EXTENT[2]))
#plt.gca().set_aspect(aspect)

plt.savefig('pic.png', dpi=980)
# plt.show()