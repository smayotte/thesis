import matplotlib.pyplot as plt
import mplleaflet
import numpy as np
import matplotlib.image as mpimg


# img = mpimg.imread('map.png')
# # left, right, bottom, top = -69.649721, -68.908044, -35.492214, -34.930012
left, right, bottom, top = -69.650, -68.907, -35.493, -34.929

fig, ax = plt.subplots(figsize=(10,10))
# # 
MAP_EXTENT = (left, right, bottom, top)
# #Turn off axis and axis ticks
ax.tick_params(labelbottom='off', labelleft='off', bottom='off', left='off')
# #import map Image
img = mpimg.imread('map.png')

ax.imshow(img, zorder=0, extent=MAP_EXTENT)
ax.axis('off')
# #no_ssd_no_pmt
# name, y, x, ID = np.loadtxt("no_ssd_no_pmt.txt",delimiter=',',unpack=True,dtype={'names': ('1', '2', '3', '4'), 'formats': (np.str, np.float, np.float, np.int)})
# ax.scatter(x, y, s=2, marker='o', color='#6f6f6f',label='without SSD\nwithout PMT')
# #ssd_no_pmt
# name, y, x, ID = np.loadtxt("ssd_no_pmt.txt",delimiter=',',unpack=True,dtype={'names': ('1', '2', '3', '4'), 'formats': (np.str, np.float, np.float, np.int)})
# ax.scatter(x, y, s=2, marker='v', label='with SSD\nwithout PMT')
# #ssd_and_pmt
# name, y, x, ID = np.loadtxt("ssd_and_pmt.txt",delimiter=',',unpack=True,dtype={'names': ('1', '2', '3', '4'), 'formats': (np.str, np.float, np.float, np.int)})
# ax.scatter(x, y, s=2, marker='^', label='with SSD\nwith PMT')


# aspect=img.shape[0]/float(img.shape[1])*((MAP_EXTENT[1]-MAP_EXTENT[0])/(MAP_EXTENT[3]-MAP_EXTENT[2]))
# ax.set_xlim(left, right)
# ax.set_ylim(bottom, top)
# plt.gca().set_aspect(aspect)
# lgd = ax.legend(title='Stations:', bbox_to_anchor=(0.8, 0.21), borderaxespad=0, ncol=1, framealpha=0.5, fontsize=8, markerscale=3, title_fontsize=8)

# fig.savefig('pic.svg', bbox_extra_artists=(lgd,), bbox_inches='tight')

# fig, ax = plt.subplots()

# ax.tick_params(labelbottom='off', labelleft='off', bottom='off', left='off')
# ax.axis('off')
#no_ssd_no_pmt
name, y, x, ID = np.loadtxt("no_ssd_no_pmt.txt",delimiter=',',unpack=True,dtype={'names': ('1', '2', '3', '4'), 'formats': (np.str, np.float, np.float, np.int)})
ax.plot(x, y,'.', markersize=4, color='#6f6f6f', zorder=1, label='without SSD\nwithout PMT')
#ssd_no_pmt
name, y, x, ID = np.loadtxt("ssd_no_pmt.txt",delimiter=',',unpack=True,dtype={'names': ('1', '2', '3', '4'), 'formats': (np.str, np.float, np.float, np.int)})
ax.plot(x, y, 'v', markersize=5,zorder=2, label='with SSD\nwithout PMT')
#ssd_and_pmt
name, y, x, ID = np.loadtxt("ssd_and_pmt.txt",delimiter=',',unpack=True,dtype={'names': ('1', '2', '3', '4'), 'formats': (np.str, np.float, np.float, np.int)})
ax.plot(x, y, '^', markersize=5,zorder=3, label='with SSD\nwith PMT')
# PPA
name, y, x, ID = np.loadtxt("PPA.txt",delimiter=',',unpack=True,dtype={'names': ('1', '2', '3', '4'), 'formats': (np.str, np.float, np.float, np.int)})
ax.plot(x, y, '+', markersize=6,zorder=4, label='with SSD\nwith PMT (PPA)')
lgd = ax.legend(title='As of 04/06/2021\nStations:', bbox_to_anchor=(0.78, 0.26), borderaxespad=0, ncol=1, framealpha=0.5, fontsize=12, title_fontsize=12)
aspect=((MAP_EXTENT[1]-MAP_EXTENT[0])/(MAP_EXTENT[3]-MAP_EXTENT[2]))
ax.set_xlim(left-0.001, right+0.001)
ax.set_ylim(bottom-0.001, top+0.001)
plt.gca().set_aspect(aspect)
fig.savefig('pic.pdf', bbox_extra_artists=(lgd,), bbox_inches='tight')
# mplleaflet.display(fig=ax.figure)
# mplleaflet.show(path='test.html')

# import staticmaps

# context = staticmaps.Context()
# context.set_tile_provider(staticmaps.tile_provider_OSM)

# #ssd_and_pmt
# name, y, x, ID = np.loadtxt("ssd_and_pmt.txt",delimiter=',',unpack=True,dtype={'names': ('1', '2', '3', '4'), 'formats': (np.str, np.float, np.float, np.int)})
# ssd_and_pmt = zip(x, y)
# #ssd_no_pmt
# name, y, x, ID = np.loadtxt("ssd_no_pmt.txt",delimiter=',',unpack=True,dtype={'names': ('1', '2', '3', '4'), 'formats': (np.str, np.float, np.float, np.int)})
# ssd_no_pmt = zip(x, y)
# #no_ssd_no_pmt
# name, y, x, ID = np.loadtxt("no_ssd_no_pmt.txt",delimiter=',',unpack=True,dtype={'names': ('1', '2', '3', '4'), 'formats': (np.str, np.float, np.float, np.int)})
# no_ssd_no_pmt = zip(x, y)

# for lat, lng in no_ssd_no_pmt:
#     context.add_object(staticmaps.CircleMarker(staticmaps.create_latlng(lng, lat), color=staticmaps.BLACK, size=5))

# for lat, lng in ssd_and_pmt:
#     context.add_object(staticmaps.TriangleMarker(staticmaps.create_latlng(lng, lat), color=staticmaps.BLUE, size=5))

# for lat, lng in ssd_no_pmt:
#     context.add_object(staticmaps.Marker(staticmaps.create_latlng(lng, lat),color=staticmaps.ORANGE,size=5))






# # render non-anti-aliased png
# image = context.render_pillow(2400, 1500)
# image.save("freiburg_area.pillow.png")

# # # # render anti-aliased png (this only works if pycairo is installed)
# # # image = context.render_cairo(2400, 1500)
# # # image.write_to_png("freiburg_area.cairo.png")

# # render svg
# svg_image = context.render_svg(1100, 1100)
# with open("freiburg_area.svg", "w", encoding="utf-8") as f:
#     svg_image.write(f, pretty=True)


# from staticmap import StaticMap, CircleMarker
# #ssd_and_pmt
# name, y, x, ID = np.loadtxt("ssd_and_pmt.txt",delimiter=',',unpack=True,dtype={'names': ('1', '2', '3', '4'), 'formats': (np.str, np.float, np.float, np.int)})
# ssd_and_pmt = zip(x, y)
# print(ssd_and_pmt)
# #ssd_no_pmt
# name, y, x, ID = np.loadtxt("ssd_no_pmt.txt",delimiter=',',unpack=True,dtype={'names': ('1', '2', '3', '4'), 'formats': (np.str, np.float, np.float, np.int)})
# ssd_no_pmt = zip(x, y)
# #no_ssd_no_pmt
# name, y, x, ID = np.loadtxt("no_ssd_no_pmt.txt",delimiter=',',unpack=True,dtype={'names': ('1', '2', '3', '4'), 'formats': (np.str, np.float, np.float, np.int)})
# no_ssd_no_pmt = zip(x, y)

# m = StaticMap(1100, 1100, 10, 10)

# for lat, lng in ssd_and_pmt:
#     marker = CircleMarker((lat, lng), , 12)
#     m.add_marker(marker)
# for lat, lng in ssd_no_pmt:
#     marker = CircleMarker((lat, lng), , 12)
#     m.add_marker(marker)
# for lat, lng in no_ssd_no_pmt:
#     marker = CircleMarker((lat, lng), , 12)
#     m.add_marker(marker)

# image = m.render(zoom=11)
# image.save('marker.svg')