# generate related variables
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.stats.stats import pearsonr
from numpy.random import randn
from numpy.random import seed

from sklearn import svm, datasets
from sklearn.metrics import plot_confusion_matrix
from sklearn.datasets import load_boston
from sklearn.linear_model import LinearRegression

def xmax_correlation(y_true, y_pred):
    reco = y_true - y_pred
    fig, ax = plt.subplots(1, figsize=(8, 8))
    corr_coeff = (round(pearsonr(y_true, y_pred)[0], 2))
    print('Correlation is: ', corr_coeff)
    binning = np.arange(min(y_true), max(y_true), 1)
    counts, xedges, yedges, im = ax.hist2d(y_true, y_pred, bins=[binning, binning], cmin=1)#, norm=mpl.colors.LogNorm())
    ax.plot([np.min(binning)-5, np.max(binning)+5], [np.min(binning)-5, np.max(binning)+5], color='grey', linestyle='--')
    ax.plot(np.NaN, np.NaN, 's', markersize=3, color='C0', label='Corr. Coeff. = ' + str(corr_coeff))
    ax.set_xlim(np.min(binning)-5, np.max(binning)+5)
    ax.set_ylim(np.min(binning)-5, np.max(binning)+5)
    ax.set_aspect('equal')
    ax.set_xlabel(r'$y_{true}$')
    ax.set_ylabel(r'$y_{DNN}$')
    ax.legend(loc='upper left')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = fig.colorbar(im, cax=cax)
    cbar.set_label('# Entries', size=14)
    cbar.ax.tick_params(labelsize=14)
    fig.savefig('Example_correlation.png')


def xmax_res_correlation(y_true, y_pred):
    reco = y_true - y_pred
    fig, axs = plt.subplots(1, 2, figsize=(15, 8), gridspec_kw={'width_ratios': [3, 1]})
    binning_y = np.arange(np.floor(np.min(reco)), np.ceil(np.max(reco)), 1)
    binning = np.arange(min(y_pred), max(y_pred), 1)
    counts, xedges, yedges, im = axs[0].hist2d(y_pred, reco, bins=[binning, binning_y], cmin=1)#, norm=mpl.colors.LogNorm())
    axs[0].plot([np.min(binning)-5, np.max(binning)+5], [0, 0], color='grey', linestyle='--')
    axs[0].plot(np.NaN, np.NaN, 's', markersize=3, color='C0')
    axs[0].set_xlim(np.min(binning)-5, np.max(binning)+5)
    axs[0].set_ylim(np.min(binning_y)-5, np.max(binning_y)+5)
    axs[0].set_aspect('equal')
    axs[0].set_ylabel(r'$y_{true} - y_{DNN}$')
    axs[0].set_xlabel(r'$y_{true}$')
    divider = make_axes_locatable(axs[0])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = fig.colorbar(im, cax=cax)
    cbar.set_label('# Entries')
    axs[1].hist(reco, bins=binning_y, orientation=u'horizontal', label=r"$\mu = $ %.2f " % np.mean(reco) +
                                                                       r"$[g/cm^{2}]$" + "\n" +
                                                                       r"$\sigma = $ %.2f " % np.std(reco) +
                                                                       r"$[g/cm^{2}]$")
    axs[1].set_ylabel(r'$y_{true} - y_{DNN}$')
    axs[1].set_xlabel('# Entries')
    axs[1].set_xscale('log')
    axs[1].set_ylim(np.min(binning_y)-5, np.max(binning_y)+5)
    fig.savefig('Example_residual_correlation.png')


linalg = np.linalg
np.random.seed(1)

num_samples = 10000
num_variables = 2
cov = [[0.3, 0.2], [0.2, 0.2]]

L = linalg.cholesky(cov)

uncorrelated = np.random.standard_normal((num_variables, num_samples))
mean = [1, 1]
correlated = np.dot(L, uncorrelated) + np.array(mean).reshape(2, 1)

xmax_correlation(100*correlated[0, :]+110, 100*correlated[1, :]+110)
xmax_res_correlation(100*correlated[0, :]+110, 100*correlated[1, :]+110)


data = load_boston()
clf = LinearRegression()
clf.fit(data.data, data.target)
predicted = clf.predict(data.data)
expected = data.target

xmax_correlation(expected,predicted)
xmax_res_correlation(expected,predicted)