import matplotlib.pyplot as plt
import numpy as np

def train_loss(values):
    return 1/(values) + 1

# def val_loss(values):
#     return 1/(values) + np.exp(0.0001*values)

def val_loss(values):
    return 1/(values) + 0.0000005*values**2 + np.exp(0.0001*values)

fig, ax = plt.subplots(figsize=(12,6))
np.random.seed(1)
values = np.arange(1, 170, 0.05)
# ax.fill_between(section,relu(section), alpha=0.5)
ax.plot(values,train_loss(values),linewidth=3, label=r'Training Dataset')
ax.plot(values,val_loss(values),linewidth=3, label=r'Validation Dataset')
# ax.axhline(1.02, color='k', linewidth=3)
# ax.arrow(175, 1.01, 0, 0.005, head_width=5, head_length=0.005, length_includes_head=False)
ax.annotate(s='', xy=(145,1.033), xytext=(145,1.006), arrowprops=dict(arrowstyle='<->'))
ax.annotate(s='Generalization Gap', xy=(110,1.012), xytext=(110,1.012))
ax.axhline(val_loss(65),xmin=0.4, color='C4', linestyle='dashdot', linewidth=3, label='OK Generalization')
ax.axvline(80, color='k', linestyle='--', linewidth=2)
ax.axvspan(0, 80, alpha=0.1, color='C3', label='Under-Fitting')
ax.axvspan(80, 170, alpha=0.1, color='C2', label='Over-Fitting')
ax.set_xlabel('Training Intervals / Epochs')
ax.set_ylabel(r'Cost Function / $J(\theta)$')
ax.legend(loc='upper right')
ax.set_ylim(0.98, 1.12)
ax.set_xlim(0.5, 170)
ax.xaxis.set_ticklabels([])
ax.yaxis.set_ticklabels([])
plt.savefig('overtraining.pdf')
