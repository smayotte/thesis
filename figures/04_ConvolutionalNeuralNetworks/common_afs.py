import matplotlib.pyplot as plt
import numpy as np

def relu(x):
    return x * (x >= 0)

def leakyrelu(x):
    return max(0.3*x, x)

def sigmoid(x):
    return 1/(1+np.exp(-x))

def tanh(x):
    return np.tanh(x)

# fig, (ax1, ax2, ax3) = plt.subplots(1, 3,figsize=(20,6))
fig, (axs) = plt.subplots(2, 2,figsize=(18,15))
section = np.arange(-5, 5, 0.1)
axs[0,0].fill_between(section,sigmoid(section), alpha=0.5)
axs[0,0].plot(section,sigmoid(section),linewidth=3, label=r'Sigmoid:' '\n' r'$\mathrm{f(x)}=\frac{1}{1+\exp(-x)}$')
axs[0,0].set_xlabel('x', fontsize=24)
axs[0,0].set_ylabel('f(x)', fontsize=24)
axs[0,0].legend(loc='upper left', fontsize=24)

axs[0,1].fill_between(section,tanh(section), alpha=0.5, label=r'TanH:' '\n' r'$\mathrm{f(x)}=\tanh(x)$')
axs[0,1].plot(section,tanh(section),linewidth=3, )
axs[0,1].set_yticks([-1, -0.5, 0, 0.5, 1])
axs[0,1].set_xlabel('x', fontsize=24)
axs[0,1].set_ylabel('f(x)', fontsize=24)
axs[0,1].legend(loc='upper left', fontsize=24)

axs[1,0].fill_between(section,relu(section), alpha=0.5)
axs[1,0].plot(section,relu(section),linewidth=3, label=r'ReLU:' '\n' r'$\mathrm{f(x)}=\max(0,x)$')
axs[1,0].set_xlabel('x', fontsize=24)
axs[1,0].set_ylabel('f(x)', fontsize=24)
axs[1,0].legend(loc='upper left', fontsize=24)

lrelu_out = [leakyrelu(x) for x in section]
axs[1,1].fill_between(section,lrelu_out, alpha=0.5)
axs[1,1].plot(section,lrelu_out,linewidth=3, label=r'Leaky ReLU:' '\n' r'$\mathrm{f(x)}=\max(\alpha x,x)$')
axs[1,1].set_xlabel('x', fontsize=24)
axs[1,1].set_ylabel('f(x)', fontsize=24)
axs[1,1].legend(loc='upper left', fontsize=24)

plt.tight_layout()
plt.savefig('common_activation_functions.pdf', transparent=True)
