import matplotlib.pyplot as plt
import numpy as np

def mse(values_true, values_pred):
    return 1/len(values_true) * np.sum((values_true-values_pred)**2)

def msle(values_true, values_pred):
    return 1/len(values_true) * np.sum((np.log(values_true)-np.log(values_pred))**2)

def mae(values_true, values_pred):
    return 1/len(values_true) * np.abs(values_true-values_pred)

fig, (ax1, ax2, ax3) = plt.subplots(1, 3,figsize=(20,6))
np.random.seed(1)
values_true = np.random.randn(1000)
values_pred = np.random.randn(1000)
ax1.fill_between(values_true,mse(values_true, values_pred), alpha=0.5)
ax1.plot(values_true,mse(values_true, values_pred),linewidth=3)#, label=r'ReLU:' '\n' r'$\mathrm{f(x)}=\max(0,x)$')
ax1.set_xlabel('x')
ax1.set_ylabel('f(x)')
ax1.legend(loc='upper left')

ax2.fill_between(values_true,msle(values_true, values_pred), alpha=0.5)
ax2.plot(values_true,msle(values_true, values_pred),linewidth=3)#, label=r'Sigmoid:' '\n' r'$\mathrm{f(x)}=\frac{1}{1+\exp(-x)}$')
ax2.set_xlabel('x')
ax2.set_ylabel('f(x)')
ax2.legend(loc='upper left')

ax3.fill_between(values_true,mae(values_true, values_pred), alpha=0.5)
ax3.plot(values_true,mae(values_true, values_pred),linewidth=3)#, label=r'TanH:' '\n' r'$\mathrm{f(x)}=\tanh(x)$')
ax3.set_xlabel('x')
ax3.set_ylabel('f(x)')
ax3.legend(loc='upper left')

plt.savefig('common_cost_functions.pdf', transparent=True)
