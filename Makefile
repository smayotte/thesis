all: build/thesis.pdf

TeXOptions = -pdf \
			 -interaction=nonstopmode \
			 -halt-on-error \
			 -output-directory=build

build/thesis.pdf: FORCE | build
	latexmk $(TeXOptions) thesis.tex
# 	makeglossaries -d build -o build/thesis.glo thesis
# 	latexmk $(TeXOptions) thesis.tex
	cp build/thesis.pdf .

quick:
	pdflatex -output-directory=build thesis.tex
	cp build/thesis.pdf .

gloss:
# 	pdflatex -output-directory=build thesis.tex
	pdflatex -synctex=1 -output-directory=build thesis.tex
	makeglossaries -d build thesis
	biber --output_directory build thesis
	pdflatex -synctex=1 -output-directory=build thesis.tex
	cp build/thesis.pdf .
	
FORCE:

build:
	mkdir -p build/

clean:
	-rm -f build/*
	-rm -f build/*/*